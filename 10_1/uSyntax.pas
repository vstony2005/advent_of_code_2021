unit uSyntax;

interface

uses
  System.Classes;

type
  TSyntax = class
  private
    FList: TStringList;
    procedure LoadDatas;
    procedure ReadDatas;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Read;
  end;

implementation

uses
  SysUtils, StrUtils;

{ TSyntax }

class procedure TSyntax.Read;
var
  syn: TSyntax;
begin
  syn := TSyntax.Create;
  try
    syn.LoadDatas;
    syn.ReadDatas;
  finally
    syn.Free;
  end;
end;

constructor TSyntax.Create;
begin
  Writeln('Subsystem syntax');
  FList := TStringList.Create;
end;

destructor TSyntax.Destroy;
begin
  FList.Free;
  inherited;
end;

procedure TSyntax.LoadDatas;
begin
  Writeln('Load datas');
  FList.LoadFromFile('..\..\input.txt');
end;

procedure TSyntax.ReadDatas;
var
  lst: TStringList;
  str, s1, lastChr: string;
  i, j, val: Integer;
  isError: Boolean;
begin
  Writeln('Read datas');
  lst := TStringList.Create;
  try
    val := 0;

    for i:=0 to FList.Count-1 do
    begin
      str := FList[i];
      lst.Clear;

      for j:=1 to Length(str) do
      begin
        s1 := str[j];
        isError := False;

        if (s1 = '(') or (s1 = '[') or (s1 = '{') or (s1 = '<') then
          lst.Add(s1)
        else if (s1 = ')') or (s1 = ']') or (s1 = '}') or (s1 = '>') then
        begin
          if (lst.Count = 0) then
            isError := True
          else
          begin
            lastChr := lst[lst.Count-1];

            if (lastChr = '(') and (s1 = ')') then
            else if (lastChr = '[') and (s1 = ']') then
            else if (lastChr = '{') and (s1 = '}') then
            else if (lastChr = '<') and (s1 = '>') then
            else
              isError := True;

            if (isError) then
            begin
              if (s1 = ')') then
                val := val + 3
              else if (s1 = ']') then
                val := val + 57
              else if (s1 = '}') then
                val := val + 1197
              else if (s1 = '>') then
                val := val + 25137;

              break;
            end
            else
              lst.Delete(lst.Count-1);
          end;
        end;
      end;  // for Length(str)
    end;  // for FList.Count

    Writeln('val: ' + IntToStr(val));
  finally
    lst.Free;
  end;
end;

end.

