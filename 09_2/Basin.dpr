program Basin;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uBasin in 'uBasin.pas';

begin
  try
    TBasin.Count;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
