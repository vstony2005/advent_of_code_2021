unit uCode;

interface

uses
  System.Classes, System.SysUtils;

type
  TCode = class
  private
    FValues: array[0..1500] of array[0..1500] of Boolean;
    FNbCol: Integer;
    FNbLin: Integer;
    FIsPart1: Boolean;
    procedure CountPoints;
    procedure LoadDatas(const AFilename: string = '');
    procedure LoadSample;
    procedure FoldX(AX: Integer);
    procedure FoldY(AY: Integer);
    procedure ReadDatas;
    procedure Print;
  public
    constructor Create(AIsPart1: Boolean);
    destructor Destroy; override;
    class procedure Read;
  end;

implementation

{ TCode }

class procedure TCode.Read;
var
  code: TCode;
begin
  // True : Part 1 - False : Part 2
  code := TCode.Create(False);
  try
    code.LoadDatas;
//    code.LoadSample;
    code.ReadDatas;
    code.CountPoints;
  finally
    code.Free;
  end;
end;

constructor TCode.Create(AIsPart1: Boolean);
begin
  FNbLin := 0;
  FNbCol := 0;
  FIsPart1 := AIsPart1;
end;

destructor TCode.Destroy;
begin
  inherited;
end;

procedure TCode.LoadDatas(const AFilename: string = '');
const
  FILE_NAME = '..\..\input.txt';
var
  lst: TStringList;
  str: string;
  idx, val1, val2: Integer;
  isReadValues: Boolean;
begin
  lst := TStringList.Create;
  try
    if (AFilename.IsEmpty) then
      lst.LoadFromFile(FILE_NAME)
    else
      lst.LoadFromFile(AFilename);

    isReadValues := False;

    for str in lst do
    begin
      if (str.IsEmpty) then
      begin
        isReadValues := True;
        Continue;
      end;

      if (isReadValues) then
      begin
        if (str.StartsWith('fold along x=')) then
          FoldX(Copy(str, 14, 4).ToInteger)
        else if (str.StartsWith('fold along y=')) then
          FoldY(Copy(str, 14, 4).ToInteger);

        if (FIsPart1) then
          Break;
      end
      else
      begin
        idx := str.IndexOf(',');
        val1 := Copy(str, 0, idx).ToInteger;
        val2 := Copy(str, idx + 2, str.Length).ToInteger;

        if (FNbLin < val2) then
          FNbLin := val2;

        if (FNbCol < val1) then
          FNbCol := val1;

        FValues[val2, val1] := True;
      end;
    end;
  finally
    lst.Free;
  end;
end;

procedure TCode.LoadSample;
const
  SAMPLE1 = '..\..\sample1.txt';
begin
  LoadDatas(SAMPLE1);
  FoldY(7);
  FoldX(5);
end;

procedure TCode.ReadDatas;
begin
  if (not FIsPart1) then
    Print;
end;

procedure TCode.FoldX(AX: Integer);
var
  i, j, col: Integer;
begin
  Writeln('x: ' + AX.ToString);

  for i:=0 to FNbLin do
  begin
    col := 0;

    for j:=AX + 1 to FNbCol do
    begin
      col := col + 1;
          FValues[i, AX - col] := FValues[i, AX - col]
        or FValues[i, AX + col];
    end;
  end;

  FNbCol := AX - 1;
end;

procedure TCode.FoldY(AY: Integer);
var
  i, j, lin: Integer;
begin
  Writeln('y: ' + AY.ToString);
  lin := 0;

  for i:=AY + 1 to FNbLin do
  begin
    lin := lin + 1;
    for j:=0 to FNbCol do
    begin
      FValues[AY - lin, j] := FValues[AY - lin, j]
        or FValues[AY + lin, j];
    end;
  end;

  FNbLin := AY - 1;
end;

procedure TCode.CountPoints;
var
  i, j, val: Integer;
begin
  Writeln('lines: ' + FNbLin.ToString);
  Writeln('cols: ' + FNbCol.ToString);
  val := 0;

  for i:=0 to FNbLin do
  begin
    for j:=0 to FNbCol do
      if (FValues[i, j]) then
        val := val + 1;
  end;

  Writeln('Val: ' + val.ToString);
end;

procedure TCode.Print;
var
  i, j: Integer;
begin
  for i:=0 to FNbLin do
  begin
    for j:=0 to FNbCol do
      if (FValues[i, j]) then
        Write('#')
      else
        Write('.');
    Writeln;
  end;
end;

end.
