unit BingoGrid;

interface

uses
  System.Classes;

type
  TBingoGrid = class
  private
    FValues: array[0..4, 0..4] of Integer;
    FFounds: array[0..4, 0..4] of Boolean;
    procedure SetLine(line: Integer; const cols: string);
  public
    procedure SetValues(const l1,l2,l3,l4,l5: string);
    function IsValuesOk: Boolean;
    procedure SetTirage(ATirage: Integer);
    function IsWin: Boolean;
    function GetValue: Integer;
    function ToStr: string;
  end;

implementation

uses
  System.SysUtils;

{ TBingoGrid }

function TBingoGrid.IsValuesOk: Boolean;
var
  i, j: Integer;
begin
  Result := False;

  for i:=0 to 4 do
    for j:=0 to 4 do
    begin
      Result := FValues[i, j] >= 0;

      if (not Result) then
        Exit;
    end;
end;

function TBingoGrid.IsWin: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=0 to 4 do
  begin
    Result := FFounds[i, 0]
              and FFounds[i, 1]
              and FFounds[i, 2]
              and FFounds[i, 3]
              and FFounds[i, 4]
              or FFounds[0, i]
              and FFounds[1, i]
              and FFounds[2, i]
              and FFounds[3, i]
              and FFounds[4, i];
    if (Result) then
      Exit;
  end;
end;

procedure TBingoGrid.SetLine(line: Integer; const cols: string);
var
  lst: TStringList;
  i: Integer;
begin
  lst := TStringList.Create;
  try
    lst.DelimitedText := cols;
    for i:=0 to lst.Count-1 do
    begin
      FValues[line, i] := StrToIntDef(lst[i], -1);
    end;
  finally
    lst.Free;
  end;
end;

procedure TBingoGrid.SetValues(const l1, l2, l3, l4, l5: string);
var
  i: Integer;
begin
  SetLine(0, l1);
  SetLine(1, l2);
  SetLine(2, l3);
  SetLine(3, l4);
  SetLine(4, l5);
end;

procedure TBingoGrid.SetTirage(ATirage: Integer);
var
  i, j: Integer;
begin
  for i:=0 to 4 do
    for j:=0 to 4 do
    begin
      if (FValues[i, j] = ATirage) then
        FFounds[i, j] := True;
    end;
end;

function TBingoGrid.GetValue: Integer;
var
  i, j: Integer;
begin
  Result := 0;

  for i:=0 to 4 do
    for j:=0 to 4 do
    begin
      if (not FFounds[i, j]) then
        Result := Result + FValues[i, j];
    end;
end;

function TBingoGrid.ToStr: string;
var
  i, j: Integer;
begin
  Result := '';

  for i:=0 to 4 do
    for j:=0 to 4 do
      Result := Result + IntToStr(FValues[i, j]) + ' ';
end;

end.
