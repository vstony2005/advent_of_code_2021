unit SpawnFish;

interface

uses
  System.Classes, System.Generics.Collections;

type
  TSpawnFish = class
  private
    FFishes: array[0..8] of Int64;
    procedure InitSample;
    procedure InitDatas;
    procedure Loop(n: Integer);
    procedure OneStep;
    procedure PrintList;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Count;
  end;

implementation

uses
  System.SysUtils;

{ TSpawnFish }

class procedure TSpawnFish.Count;
var
  fish: TSpawnFish;
begin
  fish := TSpawnFish.Create;
  try
//    fish.InitSample;
//    fish.Loop(256);

    fish.InitDatas;
    fish.Loop(256);

    fish.PrintList;
    Readln;
  finally
    fish.Free;
  end;
end;

constructor TSpawnFish.Create;
begin
end;

destructor TSpawnFish.Destroy;
begin
  inherited;
end;

procedure TSpawnFish.InitSample;
{
 18: 26
 80: 5934
}
begin
  FFishes[1] := 1;
  FFishes[2] := 1;
  FFishes[3] := 2;
  FFishes[4] := 1;
end;

procedure TSpawnFish.InitDatas;
{
  80 : 390023
}
const
  FILE_TXT = '..\..\..\06_1\input.txt';
var
  lst: TStringList;
  str: string;
  idx: Integer;
begin
  lst := TStringList.Create;
  try
    lst.LoadFromFile(FILE_TXT);
    str := lst[0];
    lst.DelimitedText := str;
    lst.Delimiter := ',';

    for str in lst do
    begin
      idx := StrToIntDef(str, -1);
      FFishes[idx] := FFishes[idx] + 1;
    end;
  finally
    lst.Free;
  end;
end;

procedure TSpawnFish.Loop(n: Integer);
var
  i: Integer;
begin
  for i:=0 to n-1 do
  begin
    Writeln(i+1);
    OneStep;
  end;
end;

procedure TSpawnFish.OneStep;
var
  newFish: Int64;
  i: Integer;
begin
  newFish := FFishes[0];

  for i:=0 to 8 do
  begin
    if (i = 8) then
      FFishes[i] := newFish
    else if (i = 6) then
      FFishes[i] := newFish + FFishes[i + 1]
    else
      FFishes[i] := FFishes[i + 1];
  end;
end;

procedure TSpawnFish.PrintList;
var
  i: Integer;
  nb: Int64;
begin
  nb := 0;

  for i:=0 to 8 do
    nb := nb + FFishes[i];

  Writeln('nb: ' + IntToStr(nb));
end;

end.
