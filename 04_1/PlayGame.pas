unit PlayGame;

interface

uses
  System.Classes,
  BingoGame;

type
  TPlayGame = class
  private
    FGame: TBingoGame;
    constructor Create;
  public
    class procedure Play;
    destructor Destroy; override;
  end;

implementation

uses
  System.SysUtils;

{ TPlayGame }

constructor TPlayGame.Create;
begin
  FGame := TBingoGame.Create;
end;

destructor TPlayGame.Destroy;
begin
  FGame.Free;
  inherited;
end;

class procedure TPlayGame.Play;
var
  play: TBingoGame;
begin
  Writeln('bingo');

  play := TBingoGame.Create;
  try
    if (play.Load) then
    begin
      writeln('load file ok');
      Writeln('grids: ' + IntToStr(play.CountGrids));
      if (play.IsGridsOk) then
      begin
        Writeln('grids ok');
        play.ExecuteTitage;
      end
      else
        Writeln('grids not ok');
    end
    else
      Writeln('error in load file');
  finally
    play.Free;
  end;

  Readln;
end;

end.
