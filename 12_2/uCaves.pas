unit uCaves;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections;

type
  TCaves = class
  private
    FCaves: TStringList;
    FLinks: TDictionary<string, string>;
    FNumPath: Int64;
    FDouble: string;
    procedure AddCaves(const s1, s2: string);
    procedure LoadDatas(const AFilename: string = '');
    procedure LoadSample;
    procedure ListPaths;
    procedure GetPath(ACurrent: TStringList; AName: string);
  public
    constructor Create;
    destructor Destroy; override;
    class procedure List;
  end;

implementation

{ TCaves }

class procedure TCaves.List;
var
  lpath: TCaves;
begin
  lpath := TCaves.Create;
  try
    lpath.LoadDatas;
//    lpath.LoadSample;
    lpath.ListPaths;
  finally
    lpath.Free;
  end;
end;

constructor TCaves.Create;
begin
  FCaves := TStringList.Create;
  FLinks := TDictionary<string, string>.Create;
  FNumPath := 0;
  FDouble := '';
end;

destructor TCaves.Destroy;
begin
  FLinks.Free;
  FCaves.Free;
  inherited;
end;

procedure TCaves.AddCaves(const s1, s2: string);
begin
  if (FCaves.IndexOf(s1) < 0) then
  begin
    FCaves.Add(s1);
    FLinks.Add(s1, s2);
  end
  else
    FLinks[s1] := FLinks[s1] + '-' + s2;

  if (FCaves.IndexOf(s2) < 0) then
  begin
    FCaves.Add(s2);
    FLinks.Add(s2, s1);
  end
  else
    FLinks[s2] := FLinks[s2] + '-' + s1;
end;

procedure TCaves.LoadDatas(const AFilename: string = '');
const
  FILE_NAME = '..\..\..\12_1\input.txt';
var
  lst: TStringList;
  str, s1, s2: string;
  i, idx: Integer;
begin
  lst := TStringList.Create;
  try
    if (AFilename.IsEmpty) then
      lst.LoadFromFile(FILE_NAME)
    else
      lst.LoadFromFile(AFilename);

    for i:=0 to lst.Count-1 do
    begin
      str := lst[i];
      idx := str.IndexOf('-');

      s1 := Copy(str, 1, idx);
      s2 := Copy(str, idx+2, Length(str));

      AddCaves(s1, s2);
    end;

    Writeln('Caves: ' + FCaves.Count.ToString);
  finally
    lst.Free;
  end;
end;

procedure TCaves.LoadSample;
const
  SAMPLE1 = '..\..\..\12_1\sample1.txt';
  SAMPLE2 = '..\..\..\12_1\sample2.txt';
  SAMPLE3 = '..\..\..\12_1\sample3.txt';
begin
  LoadDatas(SAMPLE1);
end;

procedure TCaves.ListPaths;
var
  caves: TStringList;
begin
  caves := TStringList.Create;
  try
    GetPath(caves, 'start');
  finally
    caves.Free;
  end;
end;

procedure TCaves.GetPath(ACurrent: TStringList; AName: string);
var
  lst: TStringList;
  str, newPath: string;
begin
  lst := TStringList.Create;
  try
    lst.Delimiter := '-';
    lst.DelimitedText := FLinks[AName];

    ACurrent.Add(AName);

    for str in lst do
    begin
      if (str = 'start') then
        Continue
      else if (str = 'end') then
      begin
        ACurrent.Add(str);
        FNumPath := FNumPath + 1;
        Writeln(FNumPath.ToString + ': ' + ACurrent.DelimitedText);
        ACurrent.Delete(ACurrent.Count - 1);
      end
      else
      begin
        if (str.Chars[0] in ['a'..'z'])
          and (ACurrent.IndexOf(str) >= 0) then
        begin
          if (FDouble.IsEmpty) then
            FDouble := str
          else
            Continue;
        end;

        GetPath(ACurrent, str);
      end;
    end;

    if (FDouble = ACurrent[ACurrent.Count - 1]) then
      FDouble := '';

    ACurrent.Delete(ACurrent.Count - 1);
  finally
    lst.Free;
  end;
end;

end.

