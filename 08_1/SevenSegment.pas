unit SevenSegment;

interface

uses
  System.Classes;

type
  TSevenSegment = class
  private
    procedure ReadDatas;
  public
    class procedure Count;
  end;

implementation

uses
  System.StrUtils, System.SysUtils;

{ TSevenSgment }

class procedure TSevenSegment.Count;
var
  sev: TSevenSegment;
begin
  sev := TSevenSegment.Create;
  try
    sev.ReadDatas;
    Readln;
  finally
    sev.Free;
  end;
end;

procedure TSevenSegment.ReadDatas;
const
  FILE_TXT = '..\..\input.txt';
var
  txt: Text;
  line, str: string;
  i, idx, nb: Integer;
  lst: TStringList;
begin
  Assign(txt, FILE_TXT);
  lst := TStringList.Create;
  try
    Reset(txt);
    nb := 0;

    while (not Eof(txt)) do
    begin
      Readln(txt, line);

      idx := (Pos('|', line));
      if (idx > 0) then
      begin
        str := Trim(Copy(line, idx+1, Length(line)-idx));
        lst.DelimitedText := str;

        for i:=0 to lst.Count-1 do
          case Length(lst[i]) of
            2,3,4,7:
              nb := nb + 1;
          end;
      end;
    end;

    Writeln('nb: ' + IntToStr(nb));
  finally
    lst.Free;
    CloseFile(txt);
  end;
end;

end.
