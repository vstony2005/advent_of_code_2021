program ExitCaves;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uChiton in 'uChiton.pas';

begin
  try
    TChiton.Find;
    Writeln('end');
    Readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
