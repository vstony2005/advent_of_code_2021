unit ListDigit;

interface

uses
  System.Classes, System.Generics.Collections;

type
  TValInteger = class
  public
    Value: char;
  end;

  TListDatas = class
  private
    FList: TStringList;
    FInput: TStringList;
    procedure LoadFiles;
    procedure TraitDatas;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Read;
  end;

implementation

uses
  System.SysUtils, System.StrUtils;

{ TListDigit }

constructor TListDatas.Create;
begin
  FInput := TStringList.Create;
  FList := TStringList.Create;
end;

destructor TListDatas.Destroy;
begin
  FList.Free;
  FInput.Free;
  inherited;
end;

class procedure TListDatas.Read;
var
  lDatas: TListDatas;
begin
  lDatas := TListDatas.Create;
  try
    lDatas.LoadFiles;
    lDatas.TraitDatas;

    Writeln('end');
    Readln;
  finally
    lDatas.Free;
  end;
end;

procedure TListDatas.LoadFiles;
const
  LIST_FILE = '..\..\list.txt';
  INPUT_FILE = '..\..\input_sort.txt';
var
  f: TextFile;
  line, s1: string;
  idx: Integer;
  valInt: TValInteger;
begin
  Writeln('load datas');

  FInput.LoadFromFile(INPUT_FILE);

  Assign(f, LIST_FILE);
  try
    Reset(f);

    while (not Eof(f)) do
    begin
      Readln(f, line);

      idx := Pos('=', line);
      s1 := Copy(line, 1, idx-1);

      valInt := TValInteger.Create;
      valInt.Value := Copy(line, idx+1, Length(line)).Chars[0];
      FList.AddObject(s1, valInt);
    end;
  finally
    CloseFile(f);
  end;
end;

procedure TListDatas.TraitDatas;
var
  i, idx, nb: Integer;
  str, s: string;
  lst: TStringList;
begin
  Writeln('read datas');

  lst := TStringList.Create;
  try
    s := '';
    nb := 0;
    for i:=0 to FInput.Count-1 do
    begin
      str := FInput[i];
      idx := FList.IndexOf(str);
      if (idx < 0) then
        Writeln(str + 'not found')
      else
        s := s + TValInteger(FList.Objects[idx]).Value;

      if (i mod 4) = 3 then
      begin
        nb := nb + StrToIntDef(s, 0);
        lst.Add(s);
        s := '';
      end;
    end;

    nb := nb + StrToIntDef(s, 0);
    lst.Add(s);
    Writeln(nb);

    lst.SaveToFile('datas.txt');
  finally

  end;
end;

end.
