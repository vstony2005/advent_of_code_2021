# Day 13

--- Jour 13 : Origami transparent ---

Vous atteignez une autre partie de la grotte qui est volcaniquement active. Ce serait bien si vous pouviez faire une sorte d'imagerie thermique afin de pouvoir dire à l'avance quelles grottes sont trop chaudes pour y entrer en toute sécurité.

Heureusement, le sous-marin semble être équipé d'une caméra thermique ! Lorsque vous l'activez, vous êtes accueilli par le message suivant :

Félicitations pour votre achat ! Pour activer ce système de caméra thermique infrarouge
veuillez entrer le code qui se trouve à la page 1 du manuel.

Apparemment, les Elfes n'ont jamais utilisé cette fonction. À votre grande surprise, vous parvenez à trouver le manuel ; en l'ouvrant, la page 1 tombe. C'est une grande feuille de papier transparent ! Le papier transparent est marqué de points aléatoires et comprend des instructions sur la façon de le plier (votre entrée de puzzle). Par exemple

    6,10
    0,14
    9,10
    0,3
    10,*4*
    4,11
    6,0
    6,12
    4,1
    0,13
    10,12
    3,4
    3,0
    8,4
    1,10
    2,14
    8,10
    9,0

pli le long de y=7
plier le long de x=5

La première section est une liste de points sur le papier transparent. 0,0 représente la coordonnée en haut à gauche. La première valeur, x, augmente vers la droite. La deuxième valeur, y, augmente vers le bas. Ainsi, la coordonnée 3,0 est à droite de 0,0, et la coordonnée 0,7 est en dessous de 0,0. Les coordonnées dans cet exemple forment le schéma suivant, où # est un point sur le papier et . est une position vide, non marquée :

         01234567890
    0    ...#..#..#.
    1    ....#......
    2    ...........
    3    #..........
    4    ...#....#.#
    5    ...........
    6    ...........
    7    ...........
    8    ...........
    9    ...........
    10    .#....#.##.
    11    ....#......
    12    ......#...#
    13    #..........
    14    #.#........

Ensuite, il y a une liste d'instructions de pliage. Chaque instruction indique une ligne sur le papier transparent et vous demande de plier le papier vers le haut (pour les lignes horizontales y=...) ou vers la gauche (pour les lignes verticales x=...). Dans cet exemple, la première instruction de pliage est plier le long de y=7, ce qui désigne la ligne formée par toutes les positions où y vaut 7 (marquées ici par -) :

         01234567890
    0    ...#..#..#.
    1    ....#......
    2    ...........
    3    #..........
    4    ...#....#.#
    5    ...........
    6    ...........
    7    -----------
    8    ...........
    9    ...........
    10    .#....#.##.
    11    ....#......
    12    ......#...#
    13    #..........
    14    #.#........

Comme il s'agit d'une ligne horizontale, pliez la moitié inférieure vers le haut. Certains points peuvent finir par se chevaucher une fois le pliage terminé, mais les points n'apparaîtront jamais exactement sur une ligne de pliage. Le résultat de ce pliage ressemble à ceci :

    #.##..#..#.
    #...#......
    ......#...#
    #...#......
    .#.#..#.###
    ...........
    ...........

Maintenant, seuls 17 points sont visibles.

Remarquez, par exemple, les deux points dans le coin inférieur gauche avant le pliage du papier transparent ; une fois le pliage terminé, ces points apparaissent dans le coin supérieur gauche (à 0,0 et 0,1). Comme le papier est transparent, le point situé juste en dessous dans le résultat (à 0,3) reste visible, car on peut le voir à travers le papier transparent.

Remarquez également que certains points peuvent finir par se chevaucher ; dans ce cas, les points fusionnent et deviennent un seul point.

La deuxième instruction de pliage est pliage le long de x=5, ce qui indique cette ligne :

    #.##.|#..#.
    #...#|.....
    .....|#...#
    #...#|.....
    .#.#.|#.###
    .....|.....
    .....|.....

Comme il s'agit d'une ligne verticale, pliez à gauche :

    #####
    #...#
    #...#
    #...#
    #####
    .....
    .....

Les instructions ont fait un carré !

Le papier transparent est assez grand, alors pour l'instant, concentrez-vous sur la réalisation du premier pli. Après le premier pliage dans l'exemple ci-dessus, 17 points sont visibles - les points qui se chevauchent une fois le pliage terminé comptent pour un seul point.

Combien de points sont visibles après avoir effectué le premier pli sur votre papier transparent ?

[Datas](input.txt)
