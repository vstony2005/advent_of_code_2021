program Polymerization;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uPolymer in 'uPolymer.pas';

begin
  try
    TPolymer.Read;
    Writeln('end');
    Readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
