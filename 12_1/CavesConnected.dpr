program CavesConnected;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uListPath in 'uListPath.pas';

begin
  try
    TListPath.List;
    Writeln('end');
    Readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
