program Hydrothermal;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  HydroVenture in 'HydroVenture.pas';

begin
  try
    THydroVenture.DoIt;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
