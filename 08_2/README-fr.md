# Day 8 [2]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Part Two ---

Grâce à une petite déduction, vous devriez maintenant être en mesure de déterminer les chiffres restants. Considérons à nouveau le premier exemple ci-dessus :

acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
cdfeb fcadb cdfeb cdbaf

Après une analyse approfondie, le mappage entre les fils de signaux et les segments n'a de sens que dans la configuration suivante :

     0000          dddd
    1    2        e    a
    1    2        e    a
     3333          ffff
    4    5        g    b
    4    5        g    b
     6666          cccc


Ainsi, les modèles de signaux uniques correspondraient aux chiffres suivants :

    acedgfb: 8
    cdfbe:   5
    gcdfa:   2
    fbcad:   3
    dab:     7
    cefabd:  9
    cdfgeb:  6
    eafb:    4
    cagedb:  0
    ab:      1

Ensuite, les quatre chiffres de la valeur de sortie peuvent être décodés :

    cdfeb : 5
    fcadb : 3
    cdfeb : 5
    cdbaf : 3

Par conséquent, la valeur de sortie de cette entrée est 5353.

En suivant le même processus pour chaque entrée dans le deuxième exemple plus grand ci-dessus, la valeur de sortie de chaque entrée peut être déterminée :

    fdgacbe cefdb cefbgd gcbe: 8394
    fcgedb cgb dgebacf gc:     9781
    cg cg fdcagb cbg:          1197
    efabcd cedba gadfec cb:    9361
    gecf egdcabf bgf bfgea:    4873
    gebdcfa ecba ca fadegcb:   8418
    cefg dcbef fcge gbcadfe:   4548
    ed bcgafe cdgba cbgef:     1625
    gbdfcae bgc cg cgb:        8717
    fgae cfgab fg bagce:       4315

En additionnant toutes les valeurs de sortie de ce grand exemple, on obtient 61229.

Pour chaque entrée, déterminez toutes les connexions des fils/segments et décodez les valeurs de sortie à quatre chiffres. Qu'obtenez-vous si vous additionnez toutes les valeurs de sortie ?
