unit Sliding;

interface

uses
  System.Classes;

type
  TSliding = class
  private
    procedure Count;
  public
    class procedure DoIt;
  end;

implementation

uses
  System.SysUtils;

{ TIncrement }

class procedure TSliding.DoIt;
var
  icrem: TSliding;
begin
  icrem := TSliding.Create;
  try
    icrem.Count;
  finally
    icrem.Free;
  end;
end;

procedure TSliding.Count;
const
  FILE_TXT  = '..\..\..\01\input.txt';
var
  lst: TStringList;
  val, lastVal, i: Integer;
  str: string;
  countIncr: Integer;
begin
  lst :=  TStringList.Create;
  countIncr := 0;
  try
    try
      lst.LoadFromFile(FILE_TXT);

      if (lst.Count > 2) then
        lastVal := StrToIntDef(lst[0],0)
          + StrToIntDef(lst[1],0)
          + StrToIntDef(lst[2],0);

      for i:=3 to lst.Count-1 do
      begin
        val := StrToIntDef(lst[i], 0)
               + StrToIntDef(lst[i-1], 0)
               + StrToIntDef(lst[i-2], 0);
        if (val > lastVal) then
          Inc(countIncr);
        lastVal := val;
      end;

    finally
      lst.Free;
    end;

    Writeln(Format('%d increments', [countIncr]));
  except
    Writeln('error to load file');
  end;
  Readln;
end;

end.
