unit Submarine;

interface

uses
  System.Classes;

type
  TSubmarine = class
  private
    procedure Count;
  public
    class procedure DoIt;
  end;

implementation

uses
  System.SysUtils;

{ TSubmarine }

class procedure TSubmarine.DoIt;
var
  icrem: TSubmarine;
begin
  icrem := TSubmarine.Create;
  try
    icrem.Count;
  finally
    icrem.Free;
  end;
end;

procedure TSubmarine.Count;
const
  FILE_TXT  = '..\..\input.txt';
var
  lst: TStringList;
  horizontal, depth, i: Integer;
  str: string;
  val: Integer;
begin
  lst :=  TStringList.Create;
  horizontal := 0;
  depth := 0;
  try
    try
      lst.LoadFromFile(FILE_TXT);

      for i:=0 to lst.Count-1 do
      begin
        str := lst[i];
        val := StrToIntDef(str.Substring(str.Length - 1), 0);

        if (str.StartsWith('forward')) then
          horizontal := horizontal + val
        else if (str.StartsWith('down')) then
          depth := depth + val
        else if (str.StartsWith('up')) then
          depth := depth - val;

      end;

    finally
      lst.Free;
    end;

    Writeln(Format('%d (h) * %d (d) = %d',
                   [horizontal,
                    depth,
                    horizontal*depth]));
  except
    Writeln('error to load file');
  end;
  Readln;
end;

end.
