program Subsystem;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uSyntax in 'uSyntax.pas';

begin
  try
    TSyntax.Read;
    Readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
