unit uSynchroOctos;

interface

uses
  System.Classes, System.SysUtils;

type
  TSynchroOctos = class
  private
    FIsLight: array[0..9, 0..9] of Boolean;
    FOctos: array[0..9, 0..9] of Integer;
    FNbCol: Integer;
    FNbLin: Integer;
    procedure LoadDatas;
    procedure LoadSample;
    procedure Loop;
    procedure Step;
    procedure InitLights;
    procedure AddValue(lig, col: Integer);
    procedure Propagate(lig, col: Integer);
    procedure Print;
    function IsSynchro: Boolean;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Count;
  end;

implementation

{ TSynchroOctos }

class procedure TSynchroOctos.Count;
var
  octo: TSynchroOctos;
begin
  octo := TSynchroOctos.Create;
  try
    Writeln('Bioluminescent');
    octo.LoadDatas;
//    octo.LoadSample;
    octo.Loop;
  finally
    octo.Free;
  end;
end;

constructor TSynchroOctos.Create;
begin
  FNbLin := 10;
  FNbCol := 10;
end;

destructor TSynchroOctos.Destroy;
begin
  inherited;
end;

procedure TSynchroOctos.LoadDatas;
var
  lst: TStringList;
  i, j: Integer;
  str: string;
begin
  lst := TStringList.Create;
  lst.LoadFromFile('..\..\..\11_1\input.txt');
  try
    for i:=0 to lst.Count-1 do
    begin
      str := lst[i];

      for j:=0 to Length(str) - 1 do
        FOctos[i, j] := StrToInt(str.Chars[j]);
    end;
  finally
    lst.Free;
  end;
end;

procedure TSynchroOctos.LoadSample;
var
  lst: TStringList;
  i, j: Integer;
  str: string;
begin
  lst := TStringList.Create;
  lst.LoadFromFile('..\..\..\11_1\sample2.txt');
  try
    for i:=0 to lst.Count-1 do
    begin
      str := lst[i];

      for j:=0 to Length(str) - 1 do
        FOctos[i, j] := StrToInt(str.Chars[j]);
    end;
  finally
    lst.Free;
  end;
end;

procedure TSynchroOctos.Loop;
var
  i: Integer;
begin
  Print;
  i := 0;

  while (not IsSynchro) do
  begin
    Step;
    i := i + 1;
  end;

  Print;

  Writeln('Loop: ' + IntToStr(i));
end;

procedure TSynchroOctos.Step;
var
  i, j: Integer;
begin
  InitLights;

  for i:=0 to FNbLin-1 do
    for j:=0 to FNbCol-1 do
      AddValue(i, j);

  for i:=0 to FNbLin-1 do
    for j:=0 to FNbCol-1 do
      if (FOctos[i, j] = 0) then
        Propagate(i, j);
end;

procedure TSynchroOctos.InitLights;
var
  i, j: Integer;
begin
  for i:=0 to FNbLin-1 do
    for j:=0 to FNbCol-1 do
      FIsLight[i, j] := False;
end;

procedure TSynchroOctos.AddValue(lig, col: Integer);
begin
  FOctos[lig, col] := (FOctos[lig, col] + 1) mod 10;
end;

procedure TSynchroOctos.Print;
var
  i, j: Integer;
begin
  Writeln;

  for i:=0 to FNbLin-1 do
  begin
    for j:=0 to FNbCol-1 do
      Write(FOctos[i, j]);

    Writeln;
  end;
end;

function TSynchroOctos.IsSynchro: Boolean;
var
  i, j, v: Integer;
begin
  Result := False;
  v := FOctos[0, 0];

  for i:=0 to FNbLin-1 do
    for j:=0 to FNbCol-1 do
    begin
      Result := v = FOctos[i, j];
      if (not Result) then
        Break;
    end;
end;

procedure TSynchroOctos.Propagate(lig, col: Integer);
begin
  if (FIsLight[lig, col]) then
    Exit;

  if (FOctos[lig, col] = 0) then
    FIsLight[lig, col] := True
  else
  begin
    AddValue(lig, col);

    if (FOctos[lig, col] = 0) then
      Propagate(lig, col);

    Exit;
  end;

  // FList[lig, col] = 0
  if (lig > 0) then
    Propagate(lig-1, col);

  if (lig > 0) and (col > 0) then
    Propagate(lig-1, col-1);

  if (lig > 0) and (col < FNbCol-1) then
    Propagate(lig-1, col+1);

  if (lig < FNbLin-1) then
    Propagate(lig+1, col);

  if (lig < FNbLin-1) and (col > 0) then
    Propagate(lig+1, col-1);

  if (lig < FNbLin-1) and (col < FNbCol-1) then
    Propagate(lig+1, col+1);

  if (col < FNbCol-1) then
    Propagate(lig, col+1);

  if (col > 0) then
    Propagate(lig, col-1);
end;

end.
