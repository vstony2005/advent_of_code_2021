program LanternFish256;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  SpawnFish in 'SpawnFish.pas';

begin
  try
    TSpawnFish.Count;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
