unit BingoGame;

interface

uses
  System.Classes, System.Generics.Collections,
  BingoGrid;

type
  TBingoGame = class
  private
    FGrids: TObjectList<TBingoGrid>;
    FTirages: TStringList;
    procedure AddLines(const l1,l2,l3,l4,l5: string);
    function StrTirages(const AStr: string): Boolean;
    function GetCountGrids: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function Load: Boolean;
    function IsGridsOk: Boolean;
    function ExecuteTitage: Boolean;
    property CountGrids: Integer read GetCountGrids;
  end;

implementation

uses
  System.SysUtils;

{ TBingoGame }

constructor TBingoGame.Create;
begin
  FTirages := TStringList.Create;
  FGrids := TObjectList<TBingoGrid>.Create;
end;

destructor TBingoGame.Destroy;
begin
  FGrids.Free;
  FTirages.Free;

  inherited;
end;

function TBingoGame.ExecuteTitage: Boolean;
var
  str: string;
  tirage, tirWin: Integer;
  grd, grdWin: TBingoGrid;
begin
  Result := False;

  tirWin := -1;

  for str in FTirages do
  begin
    tirage := StrToIntDef(str, -1);

    if (tirage < 0) then
    begin
      Result := False;
      Exit;
    end;

    for grd in FGrids do
    begin
      grd.SetTirage(tirage);

      if (grd.IsWin) and (not grd.GridIsWin) then
      begin
        grd.GridIsWin := True;
        tirWin := tirage;
        grdWin := grd;
      end;
    end;
  end;

  Writeln('end tirage');
  Writeln(Format('Result: %d x %d = %d',
                 [tirWin,
                  grdWin.GetValue,
                  tirWin * grdWin.GetValue]));

  Writeln;
  Writeln('end tirage');
end;

function TBingoGame.GetCountGrids: Integer;
begin
  Result := FGrids.Count;
end;

function TBingoGame.IsGridsOk: Boolean;
var
  grd: TBingoGrid;
begin
  Result := False;

  for grd in FGrids do
  begin
    Result := grd.IsValuesOk;
    if (not Result) then
      Exit;
  end;
end;

procedure TBingoGame.AddLines(const l1,l2,l3,l4,l5: string);
var
  grd: TBingoGrid;
begin
  grd := TBingoGrid.Create(FGrids.Count+1);
  grd.SetValues(l1,l2,l3,l4,l5);
  FGrids.Add(grd)
end;

function TBingoGame.Load: Boolean;
const
  DATAS: string = '..\..\..\04_1\input.txt';
var
  lst: TStringList;
  i, j: Integer;
begin
  Result := True;
  FTirages.Clear;
  lst := TStringList.Create;
  try
    try
      lst.LoadFromFile(DATAS);

      if (lst.Count > 0) and (StrTirages(lst[0])) then
      begin
        i := 2;
        while (i < lst.Count) do
        begin
          if (i+4 > lst.Count) then
          begin
            Result := False;
            Exit;
          end;

          AddLines(lst[i],
                   lst[i+1],
                   lst[i+2],
                   lst[i+3],
                   lst[i+4]);

          i := i + 6;
        end;
      end
      else
        Result := False;
    finally
      lst.Free;
    end;
  except
    on e: exception do
      Writeln(e.Message);
  end;
end;

function TBingoGame.StrTirages(const AStr: string): Boolean;
var
  i: Integer;
begin
  Result := True;
  FTirages.DelimitedText := AStr;
  if (FTirages.Count = 0) then
    Result := False
  else
    for i:=0 to FTirages.Count-1 do
    begin
      if (StrToIntDef(FTirages[i], -1) < 0) then
      begin
        Result := False;
        Break;
      end;
    end;
end;

end.
