# Day 12 [1]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Jour 12 : Passage Pathing ---

Avec les sous-systèmes souterrains de votre sous-marin qui ne fonctionnent pas de manière optimale, la seule façon de sortir de cette grotte de sitôt est de trouver un chemin vous-même. Pas seulement un chemin - la seule façon de savoir si vous avez trouvé le meilleur chemin est de les trouver tous.

Heureusement, les capteurs fonctionnent encore en grande partie, et vous pouvez donc établir une carte approximative des grottes restantes (votre entrée de puzzle). Par exemple :

    start-A
    start-b
    A-c
    A-b
    b-d
    A-end
    b-end

Voici une liste de la façon dont toutes les grottes sont reliées. Vous commencez dans la grotte nommée début, et votre destination est la grotte nommée fin. Une entrée comme b-d signifie que la grotte b est reliée à la grotte d - c'est-à-dire que vous pouvez vous déplacer entre elles.

Ainsi, le système de grottes ci-dessus ressemble à peu près à ceci :

        début
        / \
    c--A-----b--d
        \ /
        fin

Votre objectif est de trouver le nombre de chemins distincts qui commencent au début, se terminent à la fin et ne visitent pas les petites grottes plus d'une fois. Il existe deux types de grottes : les grandes grottes (écrites en majuscules, comme A) et les petites grottes (écrites en minuscules, comme b). Ce serait une perte de temps de visiter une petite grotte plus d'une fois, mais les grandes grottes sont suffisamment grandes pour que cela vaille la peine de les visiter plusieurs fois. Ainsi, tous les chemins que vous trouvez doivent visiter les petites grottes au maximum une fois, et peuvent visiter les grandes grottes autant de fois que vous le souhaitez.

Compte tenu de ces règles, il existe 10 chemins dans cet exemple de système de grottes :

    début,A,b,A,c,A,fin
    début,A,b,A,fin
    début,A,b,fin
    début,A,c,A,b,A,fin
    start,A,c,A,b,end
    start,A,c,A,end
    start,A,end
    start,b,A,c,A,end
    start,b,A,end
    début,b,fin

(Chaque ligne de la liste ci-dessus correspond à un seul chemin ; les grottes visitées par ce chemin sont listées dans l'ordre où elles sont visitées et séparées par des virgules).

Notez que dans ce système de grottes, la grotte d n'est jamais visitée par aucun chemin : pour ce faire, la grotte b devrait être visitée deux fois (une fois à l'aller et une seconde fois au retour de la grotte d), et comme la grotte b est petite, cela n'est pas permis.

Voici un exemple légèrement plus grand :

    dc-end
    HN-début
    start-kj
    dc-début
    dc-HN
    LN-dc
    HN-end
    kj-sa
    kj-HN
    kj-dc

Les 19 chemins qui le traversent sont les suivants

    début,HN,dc,HN,fin
    début,HN,dc,HN,kj,HN,fin
    début,HN,dc,fin
    début,HN,dc,kj,HN,fin
    start,HN,end
    start,HN,kj,HN,dc,HN,end
    start,HN,kj,HN,dc,end
    début,HN,kj,HN,fin
    start,HN,kj,dc,HN,end
    start,HN,kj,dc,end
    start,dc,HN,end
    start,dc,HN,kj,HN,end
    start,dc,end
    start,dc,kj,HN,end
    start,kj,HN,dc,HN,end
    start,kj,HN,dc,end
    start,kj,HN,end
    start,kj,dc,HN,end
    start,kj,dc,end

Enfin, cet exemple encore plus grand est traversé par 226 chemins :

    fs-end
    he-DX
    fs-he
    début-DX
    pj-DX
    end-zg
    zg-sl
    zg-pj
    pj-he
    RW-he
    fs-DX
    pj-RW
    zg-RW
    start-pj
    he-WI
    zg-he
    pj-fs
    start-RW

Combien y a-t-il de chemins à travers ce système de grottes qui visitent les petites grottes au plus une fois ?

[Datas](input.txt)
