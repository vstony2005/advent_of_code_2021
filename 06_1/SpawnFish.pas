unit SpawnFish;

interface

uses
  System.Classes, System.Generics.Collections;

type
  TSpawnFish = class
  private
    FList: TList<Integer>;
    procedure InitSample;
    procedure InitDatas;
    procedure Loop(n: Integer);
    procedure OneStep;
    procedure PrintList;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Count;
  end;

implementation

uses
  System.SysUtils;

{ TSpawnFish }

class procedure TSpawnFish.Count;
var
  fish: TSpawnFish;
begin
  fish := TSpawnFish.Create;
  try
    fish.InitDatas;
    fish.Loop(80);
    fish.PrintList;
    Readln;
  finally
    fish.Free;
  end;
end;

constructor TSpawnFish.Create;
begin
  FList := TList<Integer>.Create;
end;

destructor TSpawnFish.Destroy;
begin
  FList.Free;
  inherited;
end;

procedure TSpawnFish.InitDatas;
const
  FILE_TXT = '..\..\input.txt';
var
  lst: TStringList;
  str: string;
begin
  lst := TStringList.Create;
  try
    lst.LoadFromFile(FILE_TXT);
    str := lst[0];
    lst.DelimitedText := str;
    lst.Delimiter := ',';

    for str in lst do
      FList.Add(StrToIntDef(str, -1));
  finally
    lst.Free;
  end;
end;

procedure TSpawnFish.InitSample;
begin
  FList.Clear;
  FList.Add(3);
  FList.Add(4);
  FList.Add(3);
  FList.Add(1);
  FList.Add(2);
end;

procedure TSpawnFish.Loop(n: Integer);
var
  i: Integer;
begin
  for i:=0 to n-1 do
  begin
    Writeln(i+1);
    OneStep;
  end;
end;

procedure TSpawnFish.OneStep;
var
  newFish, i: Integer;
begin
  newFish := 0;

  for i:=0 to FList.Count-1 do
  begin
    FList[i] := (FList[i] - 1);

    if (FList[i] < 0) then
    begin
      FList[i] := 6;
      newFish := newFish + 1;
    end;
  end;

  for i := 1 to newFish do
    FList.Add(8);
end;

procedure TSpawnFish.PrintList;
var
  i: Integer;
begin
{
  for i in FList do
    Write(IntToStr(i) + ' ');
    }

  Writeln;
  Writeln('nb: ' + IntToStr(FList.Count));
end;

end.
