# Day 12 [2]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Deuxième partie ---

Après avoir passé en revue les chemins disponibles, vous réalisez que vous pourriez avoir le temps de visiter une seule petite grotte deux fois. Plus précisément, les grandes grottes peuvent être visitées un nombre quelconque de fois, une seule petite grotte peut être visitée au maximum deux fois, et les autres petites grottes peuvent être visitées au maximum une fois. Cependant, les grottes nommées début et fin ne peuvent être visitées qu'une seule fois chacune : une fois que vous quittez la grotte de début, vous ne pouvez pas y retourner, et une fois que vous atteignez la grotte de fin, le chemin doit se terminer immédiatement.

Maintenant, les 36 chemins possibles à travers le premier exemple ci-dessus sont :

    début,A,b,A,b,A,c,A,fin
    début,A,b,A,b,A,fin
    début,A,b,A,b,fin
    start,A,b,A,c,A,b,A,end
    start,A,b,A,c,A,b,end
    start,A,b,A,c,A,c,A,end
    start,A,b,A,c,A,end
    start,A,b,A,end
    start,A,b,d,b,A,c,A,end
    start,A,b,d,b,A,end
    start,A,b,d,b,end
    start,A,b,end
    start,A,c,A,b,A,b,A,end
    start,A,c,A,b,A,b,end
    start,A,c,A,b,A,c,A,end
    start,A,c,A,b,A,end
    start,A,c,A,b,d,b,A,end
    start,A,c,A,b,d,b,end
    start,A,c,A,b,end
    start,A,c,A,c,A,b,A,end
    start,A,c,A,c,A,b,end
    start,A,c,A,c,A,end
    start,A,c,A,end
    start,A,end
    start,b,A,b,A,c,A,end
    start,b,A,b,A,end
    start,b,A,b,end
    start,b,A,c,A,b,A,end
    start,b,A,c,A,b,end
    start,b,A,c,A,c,A,end
    start,b,A,c,A,end
    start,b,A,end
    start,b,d,b,A,c,A,end
    start,b,d,b,A,end
    start,b,d,b,end
    début,b,fin

L'exemple légèrement plus grand ci-dessus a maintenant 103 chemins qui le traversent, et l'exemple encore plus grand a maintenant 3509 chemins qui le traversent.

Compte tenu de ces nouvelles règles, combien de chemins y a-t-il dans ce système de grottes ?

[Datas](..\input\12_1)
