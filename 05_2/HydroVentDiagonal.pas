unit HydroVentDiagonal;

interface

uses
  System.Classes, System.Types;

type
  THydroVentDiagonal = class
  private
    FDatas: TStringList;
    FVents: array[0..999, 0..999] of Integer;
    procedure ReadDatas;
    procedure ListPoints(const p1, p2: TPoint);
    procedure CountVents;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure DoIt;
  end;

implementation

uses
  System.SysUtils, System.RegularExpressionsCore;

{ THydroVentDiagonal }

class procedure THydroVentDiagonal.DoIt;
var
  hydro: THydroVentDiagonal;
begin
  Writeln('Hydrothermal Venture');
  hydro := THydroVentDiagonal.Create;
  try
    hydro.ReadDatas;
  finally
    hydro.Free;
  end;
  Readln;
end;

constructor THydroVentDiagonal.Create;
begin
  FDatas := TStringList.Create;
end;

destructor THydroVentDiagonal.Destroy;
begin
  FDatas.Free;
  inherited;
end;

procedure THydroVentDiagonal.ReadDatas;
const
  TXT_DATAS = '..\..\..\05_1\input.txt';
var
  reg: TPerlRegEx;
  i, j: Integer;
  p1, p2: TPoint;
begin
  FDatas.LoadFromFile(TXT_DATAS);

  reg := TPerlRegEx.Create;
  try
    reg.RegEx := '(\d+),(\d+) \-\> (\d+),(\d+)';

    for i:=0 to FDatas.Count-1 do
    begin
      reg.Subject := FDatas[i];

      if (reg.Match) then
        if (reg.GroupCount <> 4) then
          Writeln('error : ' + FDatas[i])
        else
        begin
          p1 := Point(StrToInt(reg.Groups[1]),
                      StrToInt(reg.Groups[2]));
          p2 := Point(StrToInt(reg.Groups[3]),
                      StrToInt(reg.Groups[4]));

          ListPoints(p1, p2);
        end;
    end;

    CountVents;
  finally
    reg.Free;
  end;
end;

procedure THydroVentDiagonal.ListPoints(const p1, p2: TPoint);
// v1: 5608
var
  decalX, decalY,
  posX, posY: Integer;
begin
  if (p2.X > p1.X) then
    decalX := 1
  else if (p2.X < p1.X) then
    decalX := -1
  else
    decalX := 0;

  if (p2.Y > p1.Y) then
    decalY := 1
  else if (p2.Y < p1.Y) then
    decalY := -1
  else
    decalY := 0;

  posX := p1.X;
  posY := p1.Y;

  while (posX <> p2.X) or (posY <> p2.Y) do
  begin
    FVents[posX, posY] := FVents[posX, posY] + 1;
    posX := posX + decalX;
    posY := posY + decalY;
  end;


  FVents[p2.X, p2.Y] := FVents[p2.X, p2.Y] + 1;
end;

procedure THydroVentDiagonal.CountVents;
var
  i, j, nb: Integer;
begin
  nb := 0;
  for i:=Low(FVents) to High(FVents) do
  begin
    for j:=Low(FVents[i]) to High(FVents[i]) do
    begin
      if (FVents[i, j] > 1) then
        nb := nb + 1;
    end;
  end;

  Writeln('nb: ' + IntToStr(nb))
end;

end.
