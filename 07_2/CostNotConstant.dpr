program CostNotConstant;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  SwarmCrabs in 'SwarmCrabs.pas';

begin
  try
    TSwarmCrabs.Align;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
