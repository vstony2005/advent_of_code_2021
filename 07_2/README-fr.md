# Day 7 [2]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Deuxième partie ---

Les crabes ne semblent pas intéressés par la solution que vous proposez. Peut-être avez-vous mal compris l'ingénierie des crabes ?

Il s'avère que les moteurs des sous-marins crabes ne brûlent pas le carburant à un taux constant. Au lieu de cela, chaque changement de 1 pas dans la position horizontale coûte 1 unité de carburant de plus que le précédent : le premier pas coûte 1, le deuxième pas coûte 2, le troisième pas coûte 3, et ainsi de suite.

Au fur et à mesure que chaque crabe se déplace, aller plus loin devient plus coûteux. Cela modifie la meilleure position horizontale pour les aligner tous ; dans l'exemple ci-dessus, elle devient 5 :

- Déplacement de 16 à 5 : 66 carburant
- Déplacement de 1 à 5 : 10 carburant
- Déplacement de 2 à 5 : 6 carburant
- Déplacement de 0 à 5 : 15 carburant
- Déplacement de 4 à 5 : 1 carburant
- Déplacement de 2 à 5 : 6 carburant
- Déplacement de 7 à 5 : 3 carburant
- Déplacement de 1 à 5 : 10 carburant
- Déplacement de 2 à 5 : 6 carburant
- Déplacement de 14 à 5 : 45 carburant

Le coût total est de 168 carburant. C'est le nouveau résultat possible le moins cher ; l'ancienne position d'alignement (2) coûte désormais 206 carburant.

Déterminez la position horizontale sur laquelle les crabes peuvent s'aligner en utilisant le moins de carburant possible, afin de vous offrir une issue de secours ! Combien de carburant doivent-ils dépenser pour s'aligner sur cette position ?
