program IncrementSliding;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Sliding in 'Sliding.pas';

begin
  try
    TSliding.DoIt;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
