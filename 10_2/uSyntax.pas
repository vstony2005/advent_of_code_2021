unit uSyntax;

interface

uses
  Classes;

type
  TSyntax = class
  private
    FList: TStringList;
    procedure LoadDatas;
    procedure LoadSample;
    procedure Complete;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Read;
  end;

implementation

uses
  SysUtils, StrUtils;

{ TSyntax }

class procedure TSyntax.Read;
var
  syn: TSyntax;
begin
  syn := TSyntax.Create;
  try
    syn.LoadDatas;
//    syn.LoadSample;
    syn.Complete;
  finally
    syn.Free;
  end;
end;

constructor TSyntax.Create;
begin
  Writeln('Subsystem syntax');
  FList := TStringList.Create;
end;

destructor TSyntax.Destroy;
begin
  FList.Free;
  inherited;
end;

procedure TSyntax.LoadDatas;
begin
  Writeln('Load datas');
  FList.LoadFromFile('..\..\..\10_1\input.txt');
end;

procedure TSyntax.LoadSample;
begin
  FList.Clear;

  FList.Add('[({(<(())[]>[[{[]{<()<>>');
  FList.Add('[(()[<>])]({[<{<<[]>>(');
  FList.Add('(((({<>}<{<{<>}{[]{[]{}');
  FList.Add('{<[[]]>}<{[{[{[]{()[[[]');
  FList.Add('<{([{{}}[<[[[<>{}]]]>[]]');
end;

procedure TSyntax.Complete;
var
  lst, scores: TStringList;
  str, s1, lastChr: string;
  i, j: Integer;
  val, nb: Int64;
  isError: Boolean;
begin
  Writeln('Read datas');
  lst := TStringList.Create;
  scores := TStringList.Create;
  try
    for i:=0 to FList.Count-1 do
    begin
      str := FList[i];
      lst.Clear;
      isError := False;

      for j:=1 to Length(str) do
      begin
        s1 := str[j];
        isError := False;

        if (s1 = '(') or (s1 = '[') or (s1 = '{') or (s1 = '<') then
          lst.Add(s1)
        else if (s1 = ')') or (s1 = ']') or (s1 = '}') or (s1 = '>') then
        begin
          if (lst.Count = 0) then
            isError := True
          else
          begin
            lastChr := lst[lst.Count-1];

            if (lastChr = '(') and (s1 = ')') then
            else if (lastChr = '[') and (s1 = ']') then
            else if (lastChr = '{') and (s1 = '}') then
            else if (lastChr = '<') and (s1 = '>') then
            else
              isError := True;
          end;

          if (isError) then
          begin
            lst.Clear;
            break;
          end
          else
            lst.Delete(lst.Count-1);
        end;
      end;  // for Length(str)

      if (not isError) then
      begin
        val := 0;
        for j:=lst.Count-1 downto 0 do
        begin
          if (lst[j] = '(') then
            nb := 1
          else if (lst[j] = '[') then
            nb := 2
          else if (lst[j] = '{') then
            nb := 3
          else if (lst[j] = '<') then
            nb := 4;

          val := val * 5 + nb;
        end;

        scores.Add(IntToStr(val));
      end;
    end;  // for FList.Count

    scores.Sort;
    Writeln('scores: ' + IntToStr(scores.Count));
    scores.SaveToFile('scores.txt');
  finally
    scores.Free;
    lst.Free;
  end;
end;

end.

