unit PilotSub;

interface

uses
  System.Classes;

type
  TPilotSub = class
  private
    procedure Count;
  public
    class procedure DoIt;
  end;

implementation

uses
  System.SysUtils;

{ TPilotSub }

class procedure TPilotSub.DoIt;
var
  icrem: TPilotSub;
begin
  icrem := TPilotSub.Create;
  try
    icrem.Count;
  finally
    icrem.Free;
  end;
end;

procedure TPilotSub.Count;
const
  FILE_TXT  = '..\..\..\02_1\input.txt';
var
  lst: TStringList;
  horizontal, depth, aim, i: Integer;
  str: string;
  val: Integer;
begin
  lst :=  TStringList.Create;
  horizontal := 0;
  depth := 0;
  aim := 0;

  try
    try
      lst.LoadFromFile(FILE_TXT);

      for i:=0 to lst.Count-1 do
      begin
        str := lst[i];
        val := StrToIntDef(str.Substring(str.Length - 1), 0);

        if (str.StartsWith('forward')) then
        begin
          horizontal := horizontal + val;
          depth := depth + aim * val
        end
        else if (str.StartsWith('down')) then
          aim := aim + val
        else if (str.StartsWith('up')) then
          aim := aim - val;

      end;

    finally
      lst.Free;
    end;

    Writeln(Format('%d (h) * %d (d) = %d',
                   [horizontal,
                    depth,
                    horizontal*depth]));
  except
    Writeln('error to load file');
  end;
  Readln;
end;

end.
