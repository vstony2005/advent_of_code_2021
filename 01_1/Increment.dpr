program Increment;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  CountIncrement in 'CountIncrement.pas';

begin
  try
    TIncrement.DoIt;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
