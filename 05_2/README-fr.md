# Day 5 [2]

_Trad : deepl.com_

--- Deuxième partie ---

Malheureusement, le fait de ne considérer que les lignes horizontales et verticales ne vous donne pas une image complète ; vous devez également considérer les lignes diagonales.

En raison des limites du système de cartographie des cheminées hydrothermales, les lignes de votre liste ne seront jamais qu'horizontales, verticales, ou diagonales à exactement 45 degrés. En d'autres termes :

- Une entrée comme 1,1 -> 3,3 couvre les points 1,1, 2,2, et 3,3.
- Une entrée comme 9,7 -> 7,9 couvre les points 9,7, 8,8, et 7,9.

En considérant toutes les lignes de l'exemple ci-dessus, on obtient le diagramme suivant :

    1.1....11.
    .111...2..
    ..2.1.111.
    ...1.2.2..
    .112313211
    ...1.2....
    ..1...1...
    .1.....1..
    1.......1.
    222111....

Vous devez encore déterminer le nombre de points où au moins deux lignes se chevauchent. Dans l'exemple ci-dessus, il s'agit encore de tous les points du diagramme ayant un 2 ou plus, soit un total de 12 points.

Considérez toutes les lignes. À combien de points au moins deux lignes se chevauchent-elles ?
