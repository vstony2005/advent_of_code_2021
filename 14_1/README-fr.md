# Day 14 [1]

--- Jour 14 : Polymérisation prolongée ---

Les pressions incroyables à cette profondeur commencent à mettre à rude épreuve votre sous-marin. Le sous-marin dispose d'un équipement de polymérisation qui produirait des matériaux appropriés pour renforcer le sous-marin, et les grottes volcaniques voisines devraient même avoir les éléments d'entrée nécessaires en quantité suffisante.

Le manuel du sous-marin contient des instructions pour trouver la formule optimale de polymère ; plus précisément, il propose un modèle de polymère et une liste de règles d'insertion de paires (votre entrée de puzzle). Il vous suffit de déterminer le polymère qui sera obtenu après avoir répété plusieurs fois le processus d'insertion de paires.

Par exemple :

    NNCB

    CH -> B
    HH -> N
    CB -> H
    NH -> C
    HB -> C
    HC -> B
    HN -> C
    NN -> C
    BH -> H
    NC -> B
    NB -> B
    BN -> B
    BB -> N
    BC -> B
    CC -> N
    CN -> C

La première ligne est le modèle de polymère - c'est le point de départ du processus.

La section suivante définit les règles d'insertion des paires. Une règle comme AB -> C signifie que lorsque les éléments A et B sont immédiatement adjacents, l'élément C doit être inséré entre eux. Ces insertions se produisent toutes simultanément.

Ainsi, en commençant par le modèle de polymère NNCB, la première étape considère simultanément les trois paires :

- La première paire (NN) correspond à la règle NN -> C, l'élément C est donc inséré entre le premier N et le second N. NN C B -> NCN C B
- La deuxième paire (NC) correspond à la règle NC -> B, l'élément B est donc inséré entre le N et le C.                N NC B -> N NBC B
- La troisième paire (CB) correspond à la règle CB -> H, l'élément H est donc inséré entre le C et le B.               N N CB -> N N CHB
                                                                                                                                 NCNBCHB

Notez que ces paires se chevauchent : le deuxième élément d'une paire est le premier élément de la paire suivante. De plus, comme toutes les paires sont considérées simultanément, les éléments insérés ne sont pas considérés comme faisant partie d'une paire avant l'étape suivante.

Après la première étape de ce processus, le polymère devient NCNBCHB.

Voici les résultats de quelques étapes utilisant les règles ci-dessus :

    Modèle :     NNCB
    Après l'étape 1 : NCNBCHB
    Après l'étape 2 : NBCCNBBBCBHCB
    Après l'étape 3 : NBBBCNCCNBBNBNBBCHBHHBCHB
    Après l'étape 4 : NBBNBNBBCCNBCNCCNBBNBBNBBBBNBBCBHCBHHNHCBBCBHCB

Ce polymère se développe rapidement. Après l'étape 5, il a une longueur de 97 ; après l'étape 10, il a une longueur de 3073. Après l'étape 10, B apparaît 1749 fois, C apparaît 298 fois, H apparaît 161 fois et N apparaît 865 fois. En prenant la quantité de l'élément le plus commun (B, 1749) et en soustrayant la quantité de l'élément le moins commun (H, 161), on obtient 1749 - 161 = 1588.

Appliquez 10 étapes d'insertion de paires au modèle de polymère et trouvez les éléments les plus et les moins communs dans le résultat. Qu'obtenez-vous si vous prenez la quantité de l'élément le plus commun et soustrayez la quantité de l'élément le moins commun ?

[Datas](input.txt)
