program HydroDiagonal;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  HydroVentDiagonal in 'HydroVentDiagonal.pas';

begin
  try
    THydroVentDiagonal.DoIt;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
