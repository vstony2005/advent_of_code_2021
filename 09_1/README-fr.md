# Day [1]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Jour 9 : Smoke Basin ---

Ces grottes semblent être des tubes de lave. Certaines parties sont même encore actives sur le plan volcanique ; de petites cheminées hydrothermales libèrent de la fumée dans les grottes qui se dépose lentement comme la pluie.

Si vous pouvez modéliser la façon dont la fumée s'écoule dans les grottes, vous pourrez peut-être l'éviter et être encore plus en sécurité. Le sous-marin génère une carte de hauteur du sol des grottes voisines pour vous (votre entrée de puzzle).

La fumée s'écoule vers le point le plus bas de la zone dans laquelle elle se trouve. Par exemple, considérez la carte de hauteur suivante :

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

Chaque nombre correspond à la hauteur d'un emplacement particulier, où 9 est le plus haut et 0 le plus bas.

Votre premier objectif est de trouver les points bas, c'est-à-dire les emplacements qui sont plus bas que tous les emplacements adjacents. La plupart des emplacements ont quatre emplacements adjacents (haut, bas, gauche et droite) ; les emplacements situés sur le bord ou dans un coin de la carte ont respectivement trois ou deux emplacements adjacents. (Les emplacements en diagonale ne comptent pas comme adjacents).

Dans l'exemple ci-dessus, il y a quatre points bas, tous mis en évidence : deux sont dans la première rangée (a 1 et a 0), un est dans la troisième rangée (a 5), et un est dans la rangée du bas (également a 5). Tous les autres emplacements sur la carte de hauteur ont un emplacement adjacent plus bas, et ne sont donc pas des points bas.

Le niveau de risque d'un point bas est égal à 1 plus sa hauteur. Dans l'exemple ci-dessus, les niveaux de risque des points bas sont 2, 1, 6 et 6. La somme des niveaux de risque de tous les points bas de la carte des hauteurs est donc de 15.

Trouvez tous les points bas sur votre carte des hauteurs. Quelle est la somme des niveaux de risque de tous les points bas de votre carte des hauteurs ?


[Datas](input.txt)
