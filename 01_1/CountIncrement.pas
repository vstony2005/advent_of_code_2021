unit CountIncrement;

interface

uses
  System.Classes;

type
  TIncrement = class
  private
    procedure Count;
  public
    class procedure DoIt;
  end;

implementation

uses
  System.SysUtils;

procedure TIncrement.Count;
const
  FILE_TXT  = '..\..\input.txt';
var
  lst: TStringList;
  val, lastVal, i: Integer;
  str: string;
  countIncr: Integer;
begin
  lst :=  TStringList.Create;
  countIncr := 0;
  try
    try
      lst.LoadFromFile(FILE_TXT);

      if (lst.Count > 0) then
        lastVal := StrToIntDef(lst[0],0);

      for i:=1 to lst.Count-1 do
      begin
        val := StrToIntDef(lst[i], 0);
        if (val > lastVal) then
          Inc(countIncr);
        lastVal := val;
      end;

    finally
      lst.Free;
    end;

    Writeln(Format('%d increments', [countIncr]));
  except
    Writeln('error to load file');
  end;
  Readln;
end;

{ TIncrement }

class procedure TIncrement.DoIt;
var
  icrem: TIncrement;
begin
  icrem := TIncrement.Create;
  try
    icrem.Count;
  finally
    icrem.Free;
  end;
end;

end.
