program PilotAim;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  PilotSub in 'PilotSub.pas';

begin
  try
    TPilotSub.DoIt;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
