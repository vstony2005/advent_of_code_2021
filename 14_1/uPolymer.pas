unit uPolymer;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections;

type
  TPolymer = class
  private
    FTemplate: string;
    FRules: TDictionary<string, string>;
    procedure LoadDatas(const AFilename: string = '');
    procedure LoadSample;
    procedure Loop(Value: Integer);
    procedure Step;
    procedure CountChars;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Read;
  end;

implementation

{ TPolymer }

class procedure TPolymer.Read;
var
  poly: TPolymer;
begin
  poly := TPolymer.Create;
  try
//    poly.LoadDatas;
    poly.LoadSample;
    poly.Loop(10);
    Writeln('Length: ' + poly.FTemplate.Length.ToString);
    poly.CountChars;
  finally

  end;
end;

procedure TPolymer.CountChars;
var
  c: char;
  nbChars: TDictionary<char, Int64>;
  charMin, charMax: Int64;
begin
  nbChars := TDictionary<char, Int64>.Create;
  try
    for c in FTemplate do
    begin
      if (nbChars.ContainsKey(c)) then
        nbChars[c] := nbChars[c] + 1
      else
        nbChars.Add(c, 1);
    end;

    charMin := -1;
    charMax := -1;

    for c in nbChars.Keys do
    begin
      if (charMin < 0) then
      begin
        charMin := nbChars[c];
        charMax := nbChars[c];
      end
      else
      begin
        if (nbChars[c] > charMax) then
          charMax := nbChars[c];
        if (nbChars[c] < charMin) then
          charMin := nbChars[c];
      end;
    end;

    Writeln(Format('Result: %d - %d = %d',
                   [charMax,
                    charMin,
                    charMax - charMin]));
  finally
    nbChars.Free;
  end;
end;

constructor TPolymer.Create;
begin
  FRules := TDictionary<string, string>.Create;
  FTemplate := '';
end;

destructor TPolymer.Destroy;
begin
  FRules.Free;
  inherited;
end;

procedure TPolymer.LoadDatas(const AFilename: string = '');
const
  FILE_NAME = '..\..\input.txt';
var
  lst: TStringList;
  str, s1, s2: string;
  i: Integer;
begin
  lst := TStringList.Create;
  try
    if (AFilename.IsEmpty) then
      lst.LoadFromFile(FILE_NAME)
    else
      lst.LoadFromFile(AFilename);

    FTemplate := lst[0];

    for i:=2 to lst.Count - 1 do
    begin
      str := lst[i];

      s1 := Copy(str, 1, 2);
      s2 := Copy(str, 7, 1);

      FRules.Add(S1, S2);
    end;
  finally
    lst.Free;
  end;
end;

procedure TPolymer.LoadSample;
const
  SAMPLE = '..\..\sample.txt';
begin
  LoadDatas(SAMPLE);
end;

procedure TPolymer.Loop(Value: Integer);
var
  i: Integer;
begin
  for i:=0 to Value - 1 do
  begin
    Step;
  end;
end;

procedure TPolymer.Step;
var
  i: Int64;
  c1, c2, str: string;
  newStr: string;
begin
  newStr := '';
  for i:=0 to FTemplate.Length - 2 do
  begin
    c1 := FTemplate.Chars[i];
    c2 := FTemplate.Chars[i + 1];

    if (not FRules.TryGetValue(c1 + c2, str)) then
      str := '';

    if (i = 0) then
      newStr := c1 + str + c2
    else
      newStr := newStr + str + c2;
  end;

  FTemplate := newStr;
end;

end.
