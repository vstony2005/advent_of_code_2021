unit uBasin;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections;

type
  TBasin = class
  private
    FDatas: array[0..99, 0..99] of Integer;
    FIsCount: array[0..99, 0..99] of Boolean;
    procedure LoadDatas;
    procedure LoadSample;
    procedure ReadDatas;
    function CountZone(lig, col: Integer): Integer;
  public
    constructor Create;
    destructor Destroy; override;

    class procedure Count;
  end;

implementation

uses
  System.StrUtils;

{ TBasin }

class procedure TBasin.Count;
var
  bas: TBasin;
begin
  bas := TBasin.Create;
  try
    Writeln('Basin');
    bas.LoadDatas;
//    bas.LoadSample;
    bas.ReadDatas;
    Writeln('end');
    Readln;
  finally
    bas.Free;
  end;
end;

constructor TBasin.Create;
begin
end;

destructor TBasin.Destroy;
begin
  inherited;
end;

procedure TBasin.LoadDatas;
var
  lst: TStringList;
  lig, col: Integer;
  str: string;
begin
  Writeln('Load datas');
  lst := TStringList.Create;
  try
    lst.LoadFromFile('..\..\..\09_1\input.txt');

    for lig:=0 to lst.Count - 1 do
    begin
      str := lst[lig];
      for col:=0 to Length(str) - 1 do
      begin
        FDatas[lig, col] := StrToIntDef(str.Chars[col], 0);
        FIsCount[lig, col] := False;
      end;
    end;
  finally
    lst.Free;
  end;
end;

procedure TBasin.LoadSample;

  procedure AddStr(lig: Integer; const str: string);
  var
    i: Integer;
  begin
    for i:=0 to Length(str) - 1 do
      FDatas[lig, i] := StrToIntDef(str.Chars[i], 0);
  end;

var
  lig, col: Integer;
begin
  for lig:=0 to 99 do
    for col:=0 to 99 do
      FDatas[lig, col] := 9;

  AddStr(0, '2199943210');
  AddStr(1, '3987894921');
  AddStr(2, '9856789892');
  AddStr(3, '8767896789');
  AddStr(4, '9899965678');
end;

function TBasin.CountZone(lig, col: Integer): Integer;
var
  l, c, val: Integer;
begin
  Result := 0;
  FIsCount[lig, col] := True;

  if (FDatas[lig, col] = 9) then
    Exit;

  val := 1;

  if (col > 0) and (not FIsCount[lig, col-1]) then
    val := val + CountZone(lig, col-1);
  if (col < 99) and (not FIsCount[lig, col+1]) then
    val := val + CountZone(lig, col+1);
  if (lig > 0) and (not FIsCount[lig-1, col]) then
    val := val + CountZone(lig-1, col);
  if (lig < 99) and (not FIsCount[lig+1, col]) then
    val := val + CountZone(lig+1, col);

  if (val = 0) then
    val := 1;

  Result := val;
end;

procedure TBasin.ReadDatas;
var
  lig, col, i: Integer;
  val: Integer;
  zone: Integer;
  bigs: array[0..2] of Integer;
  mini: Integer;
begin
  val := 1;
  mini := 1;
  for i:=0 to 2 do
    bigs[i] := mini;

  for lig:=0 to 99 do
    for col:=0 to 99 do
    begin
      if (FIsCount[lig, col]) then
        Continue;

      if (FDatas[lig, col] = 9) then
      begin
        FIsCount[lig, col] := True;
        Continue;
      end;

      zone := CountZone(lig, col);

      for i:=0 to 2 do
      begin
        if (zone > bigs[i]) and (bigs[i] = mini) then
        begin
          bigs[i] := zone;
          mini := zone;
          Break;
        end;
      end;

      for i:=0 to 2 do
        if (bigs[i] < mini) then
          mini := bigs[i];
    end;

  val := bigs[0] * bigs[1] * bigs[2];

  Writeln('Value: ' + IntToStr(val));
end;

end.
