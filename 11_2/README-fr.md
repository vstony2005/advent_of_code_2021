# Day 11 [2]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Deuxième partie ---

Il semble que les flashs individuels ne soient pas assez brillants pour naviguer. Cependant, vous avez peut-être une meilleure option : les flashs semblent se synchroniser !

Dans l'exemple ci-dessus, la première fois que toutes les pieuvres clignotent simultanément est l'étape 195 :

    Après l'étape 193 :
    5877777777
    8877777777
    7777777777
    7777777777
    7777777777
    7777777777
    7777777777
    7777777777
    7777777777
    7777777777

    Après l'étape 194 :
    6988888888
    9988888888
    8888888888
    8888888888
    8888888888
    8888888888
    8888888888
    8888888888
    8888888888
    8888888888

    Après l'étape 195 :
    0000000000
    0000000000
    0000000000
    0000000000
    0000000000
    0000000000
    0000000000
    0000000000
    0000000000
    0000000000

Si tu peux calculer les moments exacts où les pieuvres clignoteront toutes simultanément, tu devrais être capable de naviguer dans la caverne. Quelle est la première étape pendant laquelle toutes les pieuvres clignotent ?

[Datas](..\11_1\input.txt)

