unit HydroVenture;

interface

uses
  System.Classes, System.Types;

type
  THydroVenture = class
  private
    FDatas: TStringList;
    FVents: array[0..999, 0..999] of Integer;
    procedure ReadDatas;
    procedure ListPoints(const p1, p2: TPoint);
    procedure PrintVents;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure DoIt;
  end;

implementation

uses
  System.SysUtils, System.RegularExpressionsCore;

{ THydroVenture }

class procedure THydroVenture.DoIt;
var
  hydro: THydroVenture;
begin
  Writeln('Hydrothermal Venture');
  hydro := THydroVenture.Create;
  try
    hydro.ReadDatas;
  finally
    hydro.Free;
  end;
  Readln;
end;

constructor THydroVenture.Create;
begin
  FDatas := TStringList.Create;
end;

destructor THydroVenture.Destroy;
begin
  FDatas.Free;
  inherited;
end;

procedure THydroVenture.ReadDatas;
const
  TXT_DATAS = '..\..\input.txt';
var
  reg: TPerlRegEx;
  i, j: Integer;
  p1, p2: TPoint;
begin
  FDatas.LoadFromFile(TXT_DATAS);

  reg := TPerlRegEx.Create;
  try
    reg.RegEx := '(\d+),(\d+) \-\> (\d+),(\d+)';

    for i:=0 to FDatas.Count-1 do
    begin
      reg.Subject := FDatas[i];

      if (reg.Match) then
        if (reg.GroupCount <> 4) then
          Writeln('error : ' + FDatas[i])
        else
        begin
          p1 := Point(StrToInt(reg.Groups[1]),
                      StrToInt(reg.Groups[2]));
          p2 := Point(StrToInt(reg.Groups[3]),
                      StrToInt(reg.Groups[4]));


          if (p1.X = p2.X) or (p1.Y = p2.Y) then
            ListPoints(p1, p2);
        end;
    end;

    PrintVents;
  finally
    reg.Free;
  end;
end;

procedure THydroVenture.ListPoints(const p1, p2: TPoint);
var
  ptMin, ptMax: TPoint;
  i: Integer;
begin
  if (p1.X + p1.Y > p2.X + p2.Y) then
  begin
    ptMin := p2;
    ptMax := p1;
  end
  else
  begin
    ptMin := p1;
    ptMax := p2;
  end;

  if (ptMax.X > ptMin.X) then
    for i:=ptMin.X to ptMax.X do
      FVents[i, ptMin.Y] := FVents[i, ptMin.Y] + 1
  else
    for i:=ptMin.Y to ptMax.Y do
      FVents[ptMin.X, i] := FVents[ptMin.X, i] + 1;
end;

procedure THydroVenture.PrintVents;
var
  i, j, nb: Integer;
begin
  nb := 0;
  for i:=Low(FVents) to High(FVents) do
  begin
    for j:=Low(FVents[i]) to High(FVents[i]) do
    begin
      if (FVents[i, j] > 1) then
        nb := nb + 1;
    end;
  end;

  Writeln('nb: ' + IntToStr(nb))
end;

end.
