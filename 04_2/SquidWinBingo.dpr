program SquidWinBingo;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  PlayGame in 'PlayGame.pas';

begin
  try
    TPlayGame.Play;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
