program Pilot;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Submarine in 'Submarine.pas';

begin
  try
    TSubmarine.DoIt;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
