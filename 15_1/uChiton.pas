unit uChiton;

interface

uses
  System.Classes, System.SysUtils;

type
  TChiton = class
  private
    FCount: Integer;
    FCaves: array of array of Integer;
    FDone: array of array of Boolean;
    FNbLin: Integer;
    FNbCol: Integer;
    procedure LoadDatas(const AFilename: string = '');
    procedure LoadSample;
    procedure StartExit;
    procedure Walk(lig, col, cost: Integer);
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Find;
  end;

implementation

uses
  System.StrUtils;

{ TChiton }

class procedure TChiton.Find;
var
  chit: TChiton;
begin
  chit := TChiton.Create;
  try
//    chit.LoadDatas;
    chit.LoadSample;
    chit.StartExit;
  finally
    chit.Free;
  end;
end;

constructor TChiton.Create;
begin
  FCount := -1;
end;

destructor TChiton.Destroy;
begin
  inherited;
end;

procedure TChiton.LoadDatas(const AFilename: string = '');
const
  FILE_NAME = '..\..\input.txt';
var
  lst: TStringList;
  str: string;
  i, j: Integer;
begin
  lst := TStringList.Create;
  try
    if (AFilename.IsEmpty) then
    begin
      FNbLin := 100;
      FNbCol := 100;
      lst.LoadFromFile(FILE_NAME);
    end
    else
      lst.LoadFromFile(AFilename);

    SetLength(FCaves, FNbLin, FNbCol);
    SetLength(FDone, FNbLin, FNbCol);

    for i:=0 to lst.Count - 1 do
    begin
      str := lst[i];

      for j:=0 to str.Length - 1 do
      begin
        FCaves[i, j] := StrToInt(str.Chars[j]);
        FDone[i, j] := False;
      end;
    end;
  finally
    lst.Free;
  end;
end;

procedure TChiton.LoadSample;
const
  SAMPLE = '..\..\sample.txt';
begin
  FNbLin := 10;
  FNbCol := 10;

  LoadDatas(SAMPLE);
end;

procedure TChiton.StartExit;
begin
  Walk(0, 0, 0);
  Writeln('Nb: ' + FCount.ToString);
end;

procedure TChiton.Walk(lig, col, cost: Integer);
var
  l, c: Integer;
  val: Integer;
begin
  if (FDone[lig, col]) then
    Exit;

  if (lig > 0) or (col > 0) then
    val := cost + FCaves[lig, col]
  else
    val := 0;

  if (FCount > 0) and (val > FCount) then
    Exit;

  if (lig = FNbLin - 1) and (col = FNbCol - 1) then
  begin
    Writeln('Val: ' + val.ToString);
    if (FCount < 0) then
      FCount := val
    else if (val < FCount) then
      FCount := val;

    Exit;
  end;

  try
    FDone[lig, col] := True;

    if (lig > 0) then
      Walk(lig-1, col, val);
    if (col > 0) then
      Walk(lig, col-1, val);
    if (lig < FNbLin-1) then
      Walk(lig+1, col, val);
    if (col < FNbCol-1) then
      Walk(lig, col+1, val);
  finally
    FDone[lig, col] := False;;
  end;
end;

end.
