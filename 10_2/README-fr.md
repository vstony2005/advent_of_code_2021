# Day 10 [2]

--- Deuxième partie ---

Maintenant, jetez les lignes corrompues. Les lignes restantes sont incomplètes.

Les lignes incomplètes n'ont pas de caractères incorrects, mais il leur manque des caractères de fermeture à la fin de la ligne. Pour réparer le sous-système de navigation, il suffit de trouver la séquence de caractères de fermeture qui complète tous les morceaux ouverts de la ligne.

Vous ne pouvez utiliser que des caractères de fermeture (`)`, `]`, `}` ou `>`), et vous devez les ajouter dans le bon ordre afin que seules les paires légales soient formées et que tous les morceaux soient fermés.

Dans l'exemple ci-dessus, il y a cinq lignes incomplètes :

- `[({(<(())[]>[[{[]{<()<>>` - Compléter en ajoutant `}}]])})]`.
- `[({([[{{`
- `[(()[<>])]({[<{<<[]>>(` - Compléter en ajoutant `)}>]})`.
- `({[<{(`
- `(((({<>}<{<{<>}{[]{[]{}` - Compléter en ajoutant `}}>}>))))`.
- `((((<{<{{`
- `{<[[]]>}<{[{[{[]{()[[[]` - Compléter en ajoutant `]]}}]}]}>`.
- `<{[{[{{[[`
- `<{([{{}}[<[[[<>{}]]]>[]]` - Compléter en ajoutant `])}>`.
- `<{([`

Saviez-vous que les outils de complétion automatique ont aussi des concours ? C'est vrai ! Le score est déterminé en considérant la chaîne de complétion caractère par caractère. Commencez par un score total de 0. Ensuite, pour chaque caractère, multipliez le score total par 5, puis augmentez le score total de la valeur de point donnée pour le caractère dans le tableau suivant :

- `)` : 1 point.
- `]` : 2 points.
- `}` : 3 points.
- `>` : 4 points.

Ainsi, la dernière chaîne d'achèvement ci-dessus - ])}> - serait notée comme suit :

- Commencez avec un score total de 0.
- Multipliez le score total par 5 pour obtenir 0, puis ajoutez la valeur de `]` (2) pour obtenir un nouveau score total de 2.
- Multipliez le score total par 5 pour obtenir 10, puis ajoutez la valeur de `)` (1) pour obtenir un nouveau score total de 11.
- Multiplier le score total par 5 pour obtenir 55, puis ajouter la valeur de `}` (3) pour obtenir un nouveau score total de 58.
- Multipliez le score total par 5 pour obtenir 290, puis ajoutez la valeur de `>` (4) pour obtenir un nouveau score total de 294.

Le score total des cinq chaînes de complétion des lignes est le suivant :

- `}}]])})]` - 288957 points au total.
- `)}>]})` - 5566 points au total.
- `}}>}>))))` - 1480781 points au total.
- `]]}}]}]}>` - 995444 points au total.
- `]]}>` - 294 points au total.

Les outils de saisie semi-automatique sont un peu particuliers : le gagnant est déterminé en triant tous les scores et en prenant le score moyen. (Il y aura toujours un nombre impair de résultats à prendre en compte.) Dans cet exemple, le résultat moyen est 288957 car il y a le même nombre de résultats plus petits et plus grands que lui.

Trouvez la chaîne de complétion pour chaque ligne incomplète, notez les chaînes de complétion et triez les scores. Quel est le score du milieu ?

[Datas](..\input.txt)
