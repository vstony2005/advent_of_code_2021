program SynchoLights;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uSynchroOctos in 'uSynchroOctos.pas';

begin
  try
    TSynchroOctos.Count;
    Writeln('end');
    Readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
