# Day 6 [1]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Jour 6 : Poisson-lanterne ---

Le fond de la mer devient plus raide. Peut-être que les clés du traîneau ont été transportées de cette façon ?

Un énorme banc de poissons-lanternes rougeoyants passe à la nage. Ils doivent se reproduire rapidement pour atteindre un tel nombre - peut-être de façon exponentielle ? Vous devriez modéliser leur taux de croissance pour en être sûr.

Bien que vous ne sachiez rien de cette espèce spécifique de poisson-lanterne, vous faites quelques suppositions sur ses attributs. Il est certain que chaque poisson-lanterne crée un nouveau poisson-lanterne une fois tous les 7 jours.

Cependant, ce processus n'est pas nécessairement synchronisé entre tous les poissons-lanternes - un poisson-lanterne peut avoir 2 jours avant de créer un autre poisson-lanterne, alors qu'un autre peut en avoir 4. Vous pouvez donc modéliser chaque poisson comme un nombre unique qui représente le nombre de jours avant qu'il ne crée un nouveau poisson-lanterne.

De plus, vous vous dites qu'un nouveau poisson-lanterne aura sûrement besoin d'un peu plus de temps avant d'être capable de produire d'autres poissons-lanternes : deux jours de plus pour son premier cycle.

Supposons donc que vous ayez un poisson-lanterne avec une valeur de minuterie interne de 3 :

- Après un jour, sa minuterie interne deviendra 2.
- Après un autre jour, sa minuterie interne deviendra 1.
- Après un autre jour, sa minuterie interne deviendra 0.
- Après un autre jour, sa minuterie interne est remise à 6 et un nouveau poisson-lanterne est créé avec une minuterie interne de 8.
- Après un autre jour, le premier poisson-lanterne aura un compteur interne de 5, et le deuxième poisson-lanterne aura un compteur interne de 7.

Un poisson-lanterne qui crée un nouveau poisson remet son compteur à 6, et non à 7 (car 0 est inclus comme valeur valide du compteur). Le nouveau poisson-lanterne commence avec un minuteur interne de 8 et ne commence à décompter que le jour suivant.

Réalisant ce que vous essayez de faire, le sous-marin produit automatiquement une liste des âges de plusieurs centaines de poissons-lanternes à proximité (votre entrée puzzle). Par exemple, supposons que l'on vous donne la liste suivante :

    3,4,3,1,2

Cette liste signifie que le premier poisson a une minuterie interne de 3, le deuxième poisson a une minuterie interne de 4, et ainsi de suite jusqu'au cinquième poisson, qui a une minuterie interne de 2. La simulation de ces poissons sur plusieurs jours se déroulerait comme suit :

    État initial   : 3,4,3,1,2
    Après  1 jour  : 2,3,2,0,1
    Après  2 jours : 1,2,1,6,0,8
    Après  3 jours : 0,1,0,5,6,7,8
    Après  4 jours : 6,0,6,4,5,6,7,8,8
    Après  5 jours : 5,6,5,3,4,5,6,7,7,8
    Après  6 jours : 4,5,4,2,3,4,5,6,6,7
    Après  7 jours : 3,4,3,1,2,3,4,5,5,6
    Après  8 jours : 2,3,2,0,1,2,3,4,4,5
    Après  9 jours : 1,2,1,6,0,1,2,3,3,4,8
    Après 10 jours : 0,1,0,5,6,0,1,2,2,3,7,8
    Après 11 jours : 6,0,6,4,5,6,0,1,1,2,6,7,8,8,8
    Après 12 jours : 5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8
    Après 13 jours : 4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8
    Après 14 jours : 3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8
    Après 15 jours : 2,3,2,0,1,2,3,4,4,5,2,3,4,4,5,5,6,6,7,7
    Après 16 jours : 1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8
    Après 17 jours : 0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8
    Après 18 jours : 6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8

Chaque jour, un 0 devient un 6 et ajoute un nouveau 8 à la fin de la liste, tandis que chaque autre chiffre diminue de 1 s'il était présent au début de la journée.

Dans cet exemple, après 18 jours, il y a un total de 26 poissons. Après 80 jours, il y aurait un total de 5934.

Trouvez un moyen de simuler des poissons-lanternes. Combien de poissons-lanternes y aurait-il après 80 jours ?
