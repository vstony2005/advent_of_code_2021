# Day 14 [2] 

--- Deuxième partie ---

Le polymère obtenu n'est pas assez solide pour renforcer le sous-marin. Vous devez effectuer plus d'étapes du processus d'insertion de paires ; un total de 40 étapes devrait suffire.

Dans l'exemple ci-dessus, l'élément le plus courant est B (présent 2192039569602 fois) et l'élément le moins courant est H (présent 3849876073 fois) ; en les soustrayant, on obtient 2188189693529.

Appliquez 40 étapes d'insertion de paires au modèle de polymère et trouvez les éléments les plus et les moins communs dans le résultat. Qu'obtenez-vous si vous prenez la quantité de l'élément le plus commun et soustrayez la quantité de l'élément le moins commun ?

