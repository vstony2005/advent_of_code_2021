unit uSmoke;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections;

type
  TSMoke = class
  private
    FDatas: TStringList;
    procedure LoadDatas;
    procedure LowPoints;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Count;
  end;

implementation

{ TSMoke }

class procedure TSMoke.Count;
var
  smok: TSMoke;
begin
  smok := TSMoke.Create;

  try
    Writeln('Smoke bassin');
    smok.LoadDatas;
    smok.LowPoints;

    Writeln('end');
    Readln;
  finally
    smok.Free;
  end;
end;

constructor TSMoke.Create;
begin
  FDatas := TStringList.Create;
end;

destructor TSMoke.Destroy;
begin
  FDatas.Free;
  inherited;
end;

procedure TSMoke.LoadDatas;
begin
  Writeln('Load datas');
  fdatas.LoadFromFile('..\..\input.txt');
end;

procedure TSMoke.LowPoints;
var
  left, up, right, down: Boolean;
  lig, col, nb: Integer;
  str: string;
  current: char;
begin
  Writeln('Count low points');

  nb := 0;

  for lig := 0 to FDatas.Count - 1 do
  begin
    str := FDatas[lig];
    for col := 0 to Length(str) - 1 do
    begin
      current := str.Chars[col];

      left := (col = 0) or (str.Chars[col - 1] > current);
      up := (lig = 0) or (FDatas[lig - 1].Chars[col] > current);
      right := (col = Length(str) - 1) or (str.Chars[col + 1] > current);
      down := (lig = FDatas.Count - 1) or (FDatas[lig + 1].Chars[col] > current);

      if left and up and right and down then
        nb := nb + StrToIntDef(current, 0) + 1;
    end;
  end;

  Writeln('nb: ' + IntToStr(nb))
end;

end.
