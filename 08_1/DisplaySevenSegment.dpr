program DisplaySevenSegment;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  SevenSegment in 'SevenSegment.pas';

begin
  try
    TSevenSegment.Count;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
