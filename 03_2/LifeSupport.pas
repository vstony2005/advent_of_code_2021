unit LifeSupport;

interface

uses
  Classes;

type
  TLifeSupport = class
  private
    procedure Count;
    function BinToDec(AStr: string): Integer;
    function GetOxygen: Integer;
    function GetCo2: Integer;
  public
    class procedure DoIt;
  end;

implementation

uses
  SysUtils, Math;

{ TLifeSupport }

class procedure TLifeSupport.DoIt;
var
  life: TLifeSupport;
begin
  life := TLifeSupport.Create;
  try
    life.Count;
  finally
    life.Free;
  end;
end;

function TLifeSupport.BinToDec(AStr: string): Integer;
var
  res, len, i: Integer;
  err: Boolean;
  str: string;
begin
  err := False;
  str := AStr;
  len := Length(str);
  res := 0;

  for i:=1 to len do
    if (str[i] = '0') or (str[i] = '1') then
      Res := Res + Trunc(Power(2, len-i)) * StrToInt(str[i])
    else
    begin
      err := True;
      Break;
    end;

  if (err) then
    Result := 0
  else
    Result := Res;
end;

procedure TLifeSupport.Count;
var
  ox, co: Integer;
begin
  ox := GetOxygen;
  co := GetCo2;

  Writeln;
  Writeln('Life support rapping');
  Writeln(Format(' >> %d * %d = %d', [ox, co, ox*co]));
  readln;
end;

function TLifeSupport.GetOxygen: Integer;
var
  lst: TStringList;

  procedure FilterList(AIndex: Integer);
  // index 1->count
  var
    nb0, nb1, i: Integer;
    str: string;
  begin
    nb0 := 0;
    nb1 := 0;

    for i:=0 to lst.Count-1 do
    begin
      str := lst[i];

      if (str[AIndex] = '0') then
        nb0 := nb0 + 1
      else if (str[AIndex] = '1') then
        nb1 := nb1 + 1;
    end;

    for i:=lst.Count-1 downto 0 do
    begin
      str := lst[i];

      if (str[AIndex] = '0') and (nb1 >= nb0) then
        lst.Delete(i)
      else if (str[AIndex] = '1') and (nb0 > nb1) then
        lst.Delete(i);
    end;
  end;

const
  FILE_TXT  = '..\03_1\input.txt';
var
  len, i: Integer;
begin
  lst :=  TStringList.Create;
  Result := -1;

  try
    try
      lst.LoadFromFile(FILE_TXT);

      if (lst.Count > 0) then
        len := Length(lst[0]);

      for i:=1 to len do
      begin
        FilterList(i);
        if (lst.Count = 1) then Break;
      end;

      if (lst.Count > 1) then
        Writeln('error > 1')
      else if (lst.Count = 1) then
      begin                                                  
        Result := BinToDec(lst[0]);
        Writeln(Format('- oxygene %s = %d', [lst[0], Result]));
      end
      else
        Writeln('error 0');
    finally
      lst.Free;
    end;
  except
    Writeln('error to load file');
  end;
end;      

function TLifeSupport.GetCo2: Integer;
var
  lst: TStringList;

  procedure FilterList(AIndex: Integer);
  // index 1->count
  var
    nb0, nb1, i: Integer;
    str: string;
  begin
    nb0 := 0;
    nb1 := 0;

    for i:=0 to lst.Count-1 do
    begin
      str := lst[i];

      if (str[AIndex] = '0') then
        nb0 := nb0 + 1
      else if (str[AIndex] = '1') then
        nb1 := nb1 + 1;
    end;

    for i:=lst.Count-1 downto 0 do
    begin
      str := lst[i];

      if (str[AIndex] = '0') and (nb0 > nb1) then
        lst.Delete(i)
      else if (str[AIndex] = '1') and (nb1 >= nb0) then
        lst.Delete(i);
    end;
  end;

const
  FILE_TXT  = '..\03_1\input.txt';
var
  len, i: Integer;
begin
  Result := -1;

  try
    lst := TStringList.Create;
    try
      lst.LoadFromFile(FILE_TXT);

      if (lst.Count > 0) then
        len := Length(lst[0]);

      for i:=1 to len do
      begin
        FilterList(i);
        if (lst.Count = 1) then Break;
      end;

      if (lst.Count > 1) then
        Writeln('error > 1')
      else if (lst.Count = 1) then
      begin                                                  
        Result := BinToDec(lst[0]);
        Writeln(Format('- co2 %s = %d', [lst[0], Result]));
      end
      else
        Writeln('error 0');
    finally
      lst.Free;
    end;
  except
    Writeln('error to load file');
  end;
end;

end.
