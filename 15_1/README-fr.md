# Day 15 [1]

--- Jour 15 : Chiton ---

Tu as presque atteint la sortie de la grotte, mais les murs se rapprochent. Votre sous-marin peut tout juste encore passer, cependant ; le principal problème est que les murs de la caverne sont couverts de chitons, et il vaudrait mieux ne pas en heurter un seul.

La caverne est grande, mais son plafond est très bas, ce qui limite vos déplacements à deux dimensions. La forme de la caverne ressemble à un carré ; une analyse rapide de la densité des chitons produit une carte du niveau de risque dans toute la caverne (votre entrée de puzzle). Par exemple :

    1163751742
    1381373672
    2136511328
    3694931569
    7463417111
    1319128137
    1359912421
    3125421639
    1293138521
    2311944581

Vous commencez en position supérieure gauche, votre destination est la position inférieure droite et vous ne pouvez pas vous déplacer en diagonale. Le chiffre correspondant à chaque position est son niveau de risque ; pour déterminer le risque total d'un chemin entier, additionnez les niveaux de risque de chaque position dans laquelle vous entrez (autrement dit, ne comptez pas le niveau de risque de votre position de départ si vous n'y entrez pas ; la quitter n'ajoute aucun risque à votre total).

Votre objectif est de trouver un chemin avec le risque total le plus faible. Dans cet exemple, le chemin présentant le risque total le plus faible est mis en évidence ici :

- **1**163751742
- **1**381373672
- **2136511**328
- 369493**15**69
- 7463417**1**11
- 1319128**13**7
- 13599124**2**1
- 31254216**3**9
- 12931385**21**
- 231194458**1**

Le risque total de ce chemin est de 40 (la position de départ n'est jamais saisie, son risque n'est donc pas compté).

Quel est le risque total le plus faible de tout chemin allant du haut à gauche au bas à droite ?

[Datas](input.txt)
