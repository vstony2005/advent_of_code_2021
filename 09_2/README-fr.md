# Jour 9 [2]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Deuxième partie ---

Ensuite, vous devez trouver les plus grands bassins afin de savoir quelles sont les zones les plus importantes à éviter.

Un bassin est l'ensemble des endroits qui s'écoulent vers le bas jusqu'à un seul point bas. Par conséquent, chaque point bas a un bassin, même si certains bassins sont très petits. Les emplacements de hauteur 9 ne comptent pas comme faisant partie d'un bassin, et tous les autres emplacements feront toujours partie d'un seul bassin.

La taille d'un bassin est le nombre d'emplacements dans le bassin, y compris le point bas. L'exemple ci-dessus comporte quatre bassins.

Le bassin en haut à gauche, de taille 3 :

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

Le bassin en haut à droite, taille 9 :

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

Le bassin moyen, taille 14 :

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

Le bassin en bas à droite, taille 9 :

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

Trouvez les trois plus grands bassins et multipliez leurs tailles ensemble. Dans l'exemple ci-dessus, cela donne 9 * 14 * 9 = 1134.

Qu'obtenez-vous si vous multipliez ensemble les tailles des trois plus grands bassins ?
