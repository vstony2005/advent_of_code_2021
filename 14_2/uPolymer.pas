unit uPolymer;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections;

type
  TPolymer = class
  private
    FTemplate: string;
    FRules: TDictionary<string, string>;
    FValues: TDictionary<string, Int64>;
    FChars: TDictionary<char, Int64>;
    FFirstChar: Char;
    FLastChar: Char;
    procedure LoadDatas(const AFilename: string = '');
    procedure LoadSample;
    procedure Loop(Value: Integer);
    procedure Step;
    procedure CountChars;
    procedure Print;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Read;
  end;

implementation

{ TPolymer }

class procedure TPolymer.Read;
var
  poly: TPolymer;
begin
  poly := TPolymer.Create;
  try
    poly.LoadDatas;
//    poly.LoadSample;
    poly.Loop(40);
  finally

  end;
end;

constructor TPolymer.Create;
begin
  FRules := TDictionary<string, string>.Create;
  FValues := TDictionary<string, Int64>.Create;
  FChars := TDictionary<char, Int64>.Create;
  FTemplate := '';
end;

destructor TPolymer.Destroy;
begin
  FChars.Free;
  FValues.Free;
  FRules.Free;
  inherited;
end;

procedure TPolymer.LoadDatas(const AFilename: string = '');
const
  FILE_NAME = '..\..\..\14_1\input.txt';
var
  lst: TStringList;
  str, s1, s2: string;
  i: Integer;
  c1, c2: Char;
begin
  lst := TStringList.Create;
  try
    if (AFilename.IsEmpty) then
      lst.LoadFromFile(FILE_NAME)
    else
      lst.LoadFromFile(AFilename);

    FTemplate := lst[0];
    FFirstChar := FTemplate.Chars[0];
    FLastChar := FTemplate.Chars[FTemplate.Length - 1];

    for i:=2 to lst.Count - 1 do
    begin
      str := lst[i];

      s1 := Copy(str, 1, 2);
      s2 := Copy(str, 7, 1);

      FRules.Add(s1, s2);
      FValues.Add(s1, 0);
    end;

    for i:=0 to FTemplate.Length - 2 do
    begin
      str := FTemplate.Chars[i] + FTemplate.Chars[i + 1];
      FValues[str] := FValues[str] + 1
    end;

    for str in FRules.Keys do
    begin
      c1 := str.Chars[0];
      c2 := str.Chars[1];

      if (not FChars.ContainsKey(c1)) then
        FChars.Add(c1, 0);
      if (not FChars.ContainsKey(c2)) then
        FChars.Add(c2, 0);
    end;
  finally
    lst.Free;
  end;
end;

procedure TPolymer.LoadSample;
const
  SAMPLE = '..\..\..\14_1\sample.txt';
begin
  LoadDatas(SAMPLE);
end;

procedure TPolymer.Loop(Value: Integer);
var
  i: Integer;
begin
  CountChars;

  for i:=0 to Value - 1 do
  begin
    Writeln('Loop: ' + (i + 1).ToString);
    Step;
    CountChars;
  end;
end;

procedure TPolymer.Step;
var
  str, s1, s2, s3: string;
  nb: Int64;
  newVals: TDictionary<string, Int64>;
begin
  newVals := TDictionary<string, Int64>.Create;
  try
    for str in FValues.Keys do
    begin
      nb := FValues[str];

      if (nb = 0) then
        Continue;

      s1 := str.Chars[0];
      s2 := str.Chars[1];
      s3 := FRules[s1 + s2];

      if (not newVals.ContainsKey(str)) then
        newVals.Add(str, 0);

      if (newVals.ContainsKey(s1 + s3)) then
        newVals[s1 + s3] := newVals[s1 + s3] + nb
      else
        newVals.Add(s1 + s3, nb);

      if (newVals.ContainsKey(s3 + s2)) then
        newVals[s3 + s2] := newVals[s3 + s2] + nb
      else
        newVals.Add(s3 + s2, nb);
    end;

    for str in newVals.Keys do
      FValues[str] := newVals[str];
  finally
    newVals.Free;
  end;
end;

procedure TPolymer.CountChars;
var
  str: string;
  len, nbMin, nbMax: Int64;
  c0, c1, c2: Char;
begin
  nbMin := -1;
  nbMax := -1;
  len := 0;

  for c0 in FChars.Keys do
    FChars[c0] := 0;

  for str in FValues.Keys do
  begin
    if (FValues[str] = 0) then
      Continue;

    c1 := str.Chars[0];
    c2 := str.Chars[1];

    FChars[c1] := FChars[c1] + FValues[str];
    FChars[c2] := FChars[c2] + FValues[str];
  end;

  for c0 in FChars.Keys do
  begin
    if (c0 = FFirstChar) or (c0 = FLastChar) then
      FChars[c0] := Trunc((FChars[c0] + 1) / 2)
    else
      FChars[c0] := Trunc(FChars[c0] / 2);
  end;

  for c0 in FChars.Keys do
  begin
    if (FChars[c0] = 0) then
      Continue;

    if (nbMin < 0) then
    begin
      nbMin := FChars[c0];
      nbMax := FChars[c0];
    end
    else
    begin
      if (FChars[c0] < nbMin) then
        nbMin := FChars[c0];

      if (FChars[c0] > nbMax) then
        nbMax := FChars[c0];
    end;
  end;

  Writeln;
  Writeln('Length: ' + (len + 1).ToString);
  Writeln(Format('Result: %d - %d = %d',
                 [nbMax,
                  nbMin,
                  nbMax - nbMin]));
  Writeln;
  Print;
end;

procedure TPolymer.Print;
var
  c1: Char;
begin
  for c1 in FChars.Keys do
  begin
    if (FChars[c1] > 0) then
      Writeln(Format('%s: %d', [c1, FChars[c1]]));
  end;
  Writeln;
end;

end.

