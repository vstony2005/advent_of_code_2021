# Day 10 [1]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Jour 10 : notation de la syntaxe ---

Vous demandez au sous-marin de déterminer la meilleure route pour sortir de la grotte sous-marine, mais il ne fait que répondre :

Erreur de syntaxe dans le sous-système de navigation sur la ligne : tous.

Tous ?! Les dégâts sont plus importants que vous ne le pensiez. Vous faites apparaître une copie du sous-système de navigation (votre entrée puzzle).

La syntaxe du sous-système de navigation est composée de plusieurs lignes contenant des chunks. Il y a un ou plusieurs chunks sur chaque ligne, et les chunks contiennent zéro ou plusieurs autres chunks. Les chunks adjacents ne sont séparés par aucun délimiteur ; si un chunk s'arrête, le chunk suivant (le cas échéant) peut immédiatement commencer. Chaque chunk doit s'ouvrir et se fermer avec l'une des quatre paires légales de caractères correspondants :

- Si un chunk s'ouvre avec `(`, il doit se fermer avec `)`.
- Si un chunk s'ouvre avec `[`, il doit se fermer avec `]`.
- Si un chunk s'ouvre avec `{`, il doit se fermer avec `}`.
- Si un chunk commence par `<`, il doit se terminer par `>`.

Ainsi, `()` est un chunk légal qui ne contient aucun autre chunk, tout comme `[]`. Des chunks plus complexes mais valides incluent `([])`, `{()()()}`, `<([{}])>`, `[<>({}){}[([])<>]]`, et même `(((((((((())))))))))`.

Certaines lignes sont incomplètes, mais d'autres sont corrompues. Trouvez et éliminez d'abord les lignes corrompues.

Une ligne corrompue est une ligne où un chunk se termine par un caractère erroné, c'est-à-dire que les caractères avec lesquels il s'ouvre et se ferme ne forment pas l'une des quatre paires légales énumérées ci-dessus.

Parmi les exemples de chunks corrompus, citons `(]`, `{()()()>`, `(((()))}`, et `<([]){()}[{}])`. Un tel fragment peut apparaître n'importe où dans une ligne, et sa présence fait que la ligne entière est considérée comme corrompue.

Par exemple, considérons le sous-système de navigation suivant :

    [({(<(())[]>[[{[]{<()<>>
    [(()[<>])]({[<{<<[]>>(
    {([(<{}[<>[]}>{[]{[(<()>
    (((({<>}<{<{<>}{[]{[]{}
    [[<[([]))<([[{}[[()]]]
    [{[{({}]{}}([{[{{{}}([]
    {<[[]]>}<{[{[{[]{()[[[]
    [<(<(<(<{}))><([]([]()
    <{([([[(<>()){}]>(<<{{
    <{([{{}}[<[[[<>{}]]]>[]]

Certaines des lignes ne sont pas corrompues, juste incomplètes ; vous pouvez ignorer ces lignes pour le moment. Les cinq lignes restantes sont corrompues :

- `{([(<{}[<>[]}>{[]{[(<()>` - Attendu `]`, mais trouvé `}` à la place.
- `.......[....}...........`
- `[[<[([]))<([{}[[()]]]` - Attendu `]`, mais trouvé `)` au lieu de.
- `...[....).............`
- `[{[{({}]{}}([{{{{}}([]` - Attendu `)`, mais trouvé `]` à la place.
- `....(..]...............`
- `[<(<(<(<{})))><([]([]()` - Attendu `>`, mais trouvé `)` au lieu de.
- `<{(([[(<>()){}]>(<<{{` - Attendu `]`, mais trouvé `>` à la place.

Arrêtez-vous au premier caractère de fermeture incorrect sur chaque ligne corrompue.

Saviez-vous que les vérificateurs de syntaxe organisent en fait des concours pour voir qui peut obtenir le meilleur score pour les erreurs de syntaxe dans un fichier ? C'est bien vrai ! Pour calculer le score d'erreurs syntaxiques d'une ligne, prenez le premier caractère illégal de la ligne et consultez le tableau suivant :

- `)` : 3 points.
- `]` : 57 points.
- `}` : 1197 points.
- `>` : 25137 points.

Dans l'exemple ci-dessus, un `)` illégal a été trouvé deux fois (2*3 = 6 points), un `]` illégal a été trouvé une fois (57 points), un `}` illégal a été trouvé une fois (1197 points), et un `>` illégal a été trouvé une fois (25137 points). Ainsi, le score total d'erreurs syntaxiques pour ce fichier est de 6+57+1197+25137 = 26397 points !

Trouvez le premier caractère illégal dans chaque ligne corrompue du sous-système de navigation. Quel est le score total d'erreurs syntaxiques pour ces erreurs ?

[Datas](input.txt)
