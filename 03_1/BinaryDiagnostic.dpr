program BinaryDiagnostic;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  BinDiagnostic in 'BinDiagnostic.pas';

begin
  try
    TBinDiagnostic.DoIt;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
