program SmokeBassin;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uSmoke in 'uSmoke.pas';

begin
  try
    TSMoke.Count;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
