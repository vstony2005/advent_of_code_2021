unit SwarmCrabs;

interface

uses
  System.Classes, System.Generics.Collections;

type
  TSwarmCrabs = class
  private
    FPosition: TList<Integer>;
    procedure InitSample;
    procedure InitDatas;
    procedure TestPositions;
    function GetCost(p1, p2: Integer): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Align;
  end;

implementation

uses
  System.SysUtils;

{ TSwarmCrabs }

class procedure TSwarmCrabs.Align;
var
  crabs: TSwarmCrabs;
begin
  crabs := TSwarmCrabs.Create;
  try
    crabs.InitDatas;
    crabs.TestPositions;

    Readln;
  finally
    crabs.Free;
  end;
end;

constructor TSwarmCrabs.Create;
begin
  FPosition := TList<Integer>.Create;
end;

destructor TSwarmCrabs.Destroy;
begin
  FPosition.Free;
  inherited;
end;

procedure TSwarmCrabs.InitDatas;
const
  FILE_TXT = '..\..\..\07_1\input.txt';
var
  lst: TStringList;
  str: string;
begin
  lst := TStringList.Create;
  try
    lst.LoadFromFile(FILE_TXT);
    lst.DelimitedText := lst[0];
    lst.Delimiter := ',';

    for str in lst do
      FPosition.Add(StrToIntDef(str, -1));
  finally
    lst.Free;
  end;
end;

procedure TSwarmCrabs.InitSample;
begin
  FPosition.Add(16);
  FPosition.Add(1);
  FPosition.Add(2);
  FPosition.Add(0);
  FPosition.Add(4);
  FPosition.Add(2);
  FPosition.Add(7);
  FPosition.Add(1);
  FPosition.Add(2);
  FPosition.Add(14);
end;

function TSwarmCrabs.GetCost(p1, p2: Integer): Integer;
var
  i, nb: Integer;
begin
  nb := Abs(p1 - p2);

  Result := 0;
  for i:=1 to nb do
    Result := Result + i;
end;

procedure TSwarmCrabs.TestPositions;
var
  costFuel: TDictionary<Integer, Integer>;
  cost: Integer;
  i, j,
  posMax,
  valMin, posMin: Integer;
begin
  costFuel := TDictionary<Integer, Integer>.Create;
  try
    posMax := 0;
    for i:=0 to FPosition.Count-1 do
      if (FPosition[i] > posMax) then
        posMax := FPosition[i];

    for i:=0 to posMax-1 do
    begin
      if (costFuel.ContainsKey(i)) then
        continue;

      costFuel.Add(i, 0);

      for j:=0 to FPosition.Count-1 do
      begin
        costFuel[i] := costFuel[i] + GetCost(i, FPosition[j])
      end;
    end;

    valMin := MaxInt;
    for i in costFuel.Keys do
      if (costFuel[i] < valMin) then
      begin
        valMin := costFuel[i];
        posMin := i;
      end;

    Writeln(Format(
                   'min cost = %d - position = %d',
                   [valMin,
                    posMin]));
  finally
    costFuel.Free;
  end;
end;

end.
