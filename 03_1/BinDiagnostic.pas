unit BinDiagnostic;

interface

uses
  System.Classes;

type
  TBinDiagnostic = class
  private
    procedure Count;
    function BinToDec(AStr: string): Integer;
  public
    class procedure DoIt;
  end;

implementation

uses
  System.SysUtils, System.Math;

{ TBinDiagnostic }

class procedure TBinDiagnostic.DoIt;
var
  diag: TBinDiagnostic;
begin
  diag := TBinDiagnostic.Create;
  try
    diag.Count;
  finally
    diag.Free;
  end;
end;

function TBinDiagnostic.BinToDec(AStr: string): Integer;
var
  res, len, i: Integer;
  err: Boolean;
  str: string;
begin
  err := False;
  str := AStr;
  len := str.Length;
  res := 0;

  for i:=0 to len-1 do
    if (str.Chars[i] = '0') or (str.Chars[i] = '1') then
      Res := Res + Trunc(Power(2, len-i-1)) * StrToInt(str.Chars[i])
    else
    begin
      err := True;
      Break;
    end;

  if (err) then
    Result := 0
  else
    Result := Res;
end;

procedure TBinDiagnostic.Count;
const
  FILE_TXT  = '..\..\input.txt';
var
  lst: TStringList;
  nb0, nb1: array of Integer;
  i, j: Integer;
  str, sGam, sEps: string;
  gamma, epsilon: Integer;
begin
  lst :=  TStringList.Create;
  sGam :=  '';
  sEps := '';

  try
    try
      lst.LoadFromFile(FILE_TXT);
      if (lst.Count > 0) then
      begin
        SetLength(nb0, lst[0].Length);
        SetLength(nb1, lst[0].Length);
      end;

      for i:=0 to lst.Count-1 do
      begin
        str := lst[i];

        for j:=0 to str.Length-1 do
        begin
          if (str.Chars[j] = '0') then
            nb0[j] := nb0[j] + 1
          else if (str.Chars[j] = '1') then
            nb1[j] := nb1[j] + 1;
        end;
      end;

      for i:=low(nb0) to high(nb0) do
      begin
        if (nb0[i] > nb1[i]) then
        begin
          sGam := sGam + '0';
          sEps := sEps + '1';
        end
        else
        begin
          sGam := sGam + '1';
          sEps := sEps + '0';
        end;
      end;

      gamma := BinToDec(sGam);
      epsilon := BinToDec(sEps);
    finally
      lst.Free;
    end;

    Writeln(Format('gamma (%s) - epsilon (%s)',
                   [sGam, sEps]));
    Writeln(Format('gamma (%d) - epsilon (%d)',
                   [gamma, epsilon]));
    Writeln(Format('result = %d',
                   [gamma * epsilon]));
  except
    Writeln('error to load file');
  end;
  Readln;
end;

end.
