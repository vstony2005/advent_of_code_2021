# Day 7 [1]

_Traduit avec www.DeepL.com/Translator (version gratuite)_

--- Jour 7 : La traîtrise des baleines ---

Une baleine géante a décidé que votre sous-marin serait son prochain repas, et elle est beaucoup plus rapide que vous. Il n'y a nulle part où aller !

Soudain, un essaim de crabes (chacun dans son propre petit sous-marin - sinon, c'est trop profond pour eux) arrive en trombe pour te sauver ! Ils semblent se préparer à faire un trou dans le fond de l'océan ; les capteurs indiquent un énorme système de grottes souterraines juste au-delà de l'endroit visé !

Les sous-marins crabes doivent tous être alignés avant d'avoir assez de puissance pour faire un trou assez large pour que votre sous-marin puisse passer. Cependant, il semble qu'ils ne seront pas alignés avant que la baleine ne vous attrape ! Peut-être pouvez-vous aider ?

Il y a un problème majeur : les sous-marins crabes ne peuvent se déplacer qu'horizontalement.

Vous devez rapidement dresser une liste de la position horizontale de chaque crabe (votre entrée de puzzle). Les sous-marins crabes ont un carburant limité, vous devez donc trouver un moyen de faire correspondre toutes leurs positions horizontales tout en leur demandant de dépenser le moins de carburant possible.

Par exemple, considérez les positions horizontales suivantes :

    16,1,2,0,4,2,7,1,2,14

Cela signifie qu'il y a un crabe avec la position horizontale 16, un crabe avec la position horizontale 1, et ainsi de suite.

Chaque changement de 1 pas dans la position horizontale d'un seul crabe coûte 1 carburant. Vous pourriez choisir n'importe quelle position horizontale pour les aligner tous, mais celle qui coûte le moins de carburant est la position horizontale 2 :

- Déplacement de 16 à 2 : 14 carburant
- Déplacement de 1 à 2 : 1 carburant
- Déplacement de 2 à 2 : 0 carburant
- Déplacement de 0 à 2 : 2 carburant
- Déplacement de 4 à 2 : 2 carburant
- Déplacement de 2 à 2 : 0 carburant
- Déplacement de 7 à 2 : 5 carburant
- Déplacement de 1 à 2 : 1 carburant
- Passer de 2 à 2 : 0 carburant
- Déplacement de 14 à 2 : 12 carburant

Le coût total est de 37 carburant. Il s'agit de l'issue la moins chère possible ; les issues plus coûteuses comprennent l'alignement sur la position 1 (41 carburant), la position 3 (39 carburant) ou la position 10 (71 carburant).

Déterminez la position horizontale sur laquelle les crabes peuvent s'aligner en utilisant le moins de carburant possible. Combien de carburant doivent-ils dépenser pour s'aligner sur cette position ?

[Datas](input.txt)
