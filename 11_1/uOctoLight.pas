unit uOctoLight;

interface

uses
  System.Classes, System.SysUtils;

type
  TOctoLight = class
  private
    FIsLight: array[0..9, 0..9] of Boolean;
    FOctos: array[0..9, 0..9] of Integer;
    FValue: Int64;
    FNbCol: Integer;
    FNbLin: Integer;
    procedure LoadDatas;
    procedure LoadSample;
    procedure Loop(nb: Integer);
    procedure Step;
    procedure InitLights;
    procedure AddValue(lig, col: Integer);
    procedure Propagate(lig, col: Integer);
    procedure Print;
  public
    constructor Create;
    destructor Destroy; override;
    class procedure Light;
  end;

implementation

{ TOctoLight }

class procedure TOctoLight.Light;
var
  octo: TOctoLight;
begin
  octo := TOctoLight.Create;
  try
    Writeln('Bioluminescent');
    octo.LoadDatas;
//    octo.LoadSample;
    octo.Loop(100);
  finally
    octo.Free;
  end;
end;

constructor TOctoLight.Create;
begin
  FNbLin := 10;
  FNbCol := 10;
  FValue := 0;
end;

destructor TOctoLight.Destroy;
begin
  inherited;
end;

procedure TOctoLight.LoadDatas;
var
  lst: TStringList;
  i, j: Integer;
  str: string;
begin
  lst := TStringList.Create;
  lst.LoadFromFile('..\..\input.txt');
  try
    for i:=0 to lst.Count-1 do
    begin
      str := lst[i];

      for j:=0 to Length(str) - 1 do
        FOctos[i, j] := StrToInt(str.Chars[j]);
    end;
  finally
    lst.Free;
  end;
end;

procedure TOctoLight.LoadSample;
var
  lst: TStringList;
  i, j: Integer;
  str: string;
begin
  lst := TStringList.Create;
  lst.LoadFromFile('..\..\sample2.txt');
  try
    for i:=0 to lst.Count-1 do
    begin
      str := lst[i];

      for j:=0 to Length(str) - 1 do
        FOctos[i, j] := StrToInt(str.Chars[j]);
    end;
  finally
    lst.Free;
  end;
end;

procedure TOctoLight.Loop(nb: Integer);
var
  i: Integer;
begin
  Print;
  for i:=0 to nb-1 do
  begin
    Step;
  end;

  Writeln('Value: ' + IntToStr(FValue));
end;

procedure TOctoLight.Step;
var
  i, j: Integer;
begin
  InitLights;

  for i:=0 to FNbLin-1 do
    for j:=0 to FNbCol-1 do
      AddValue(i, j);

  for i:=0 to FNbLin-1 do
    for j:=0 to FNbCol-1 do
      if (FOctos[i, j] = 0) then
        Propagate(i, j);
end;

procedure TOctoLight.InitLights;
var
  i, j: Integer;
begin
  for i:=0 to FNbLin-1 do
    for j:=0 to FNbCol-1 do
      FIsLight[i, j] := False;
end;

procedure TOctoLight.AddValue(lig, col: Integer);
begin
  FOctos[lig, col] := (FOctos[lig, col] + 1) mod 10;
end;

procedure TOctoLight.Print;
var
  i, j: Integer;
begin
  Writeln;

  for i:=0 to FNbLin-1 do
  begin
    for j:=0 to FNbCol-1 do
      Write(FOctos[i, j]);

    Writeln;
  end;
end;

procedure TOctoLight.Propagate(lig, col: Integer);
begin
  if (FIsLight[lig, col]) then
    Exit;

  if (FOctos[lig, col] = 0) then
  begin
    FIsLight[lig, col] := True;
    FValue := FValue + 1;
  end
  else
  begin
    AddValue(lig, col);

    if (FOctos[lig, col] = 0) then
      Propagate(lig, col);

    Exit;
  end;

  // FList[lig, col] = 0
  if (lig > 0) then
    Propagate(lig-1, col);

  if (lig > 0) and (col > 0) then
    Propagate(lig-1, col-1);

  if (lig > 0) and (col < FNbCol-1) then
    Propagate(lig-1, col+1);

  if (lig < FNbLin-1) then
    Propagate(lig+1, col);

  if (lig < FNbLin-1) and (col > 0) then
    Propagate(lig+1, col-1);

  if (lig < FNbLin-1) and (col < FNbCol-1) then
    Propagate(lig+1, col+1);

  if (col < FNbCol-1) then
    Propagate(lig, col+1);

  if (col > 0) then
    Propagate(lig, col-1);
end;

end.
