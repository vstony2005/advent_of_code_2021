unit uListPath;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections;

type
  TListPath = class
  private
    FCaves: TStringList;
    FLinks: TDictionary<string, string>;
    FNumPath: Integer;
    procedure AddCaves(const s1, s2: string);
    procedure LoadDatas(const AFilename: string = '');
    procedure LoadSample;
    procedure ListPaths;
    procedure GetPath(ACurrent: TStringList; AName: string);
  public
    constructor Create;
    destructor Destroy; override;
    class procedure List;
  end;

implementation

{ TListPath }

class procedure TListPath.List;
var
  lpath: TListPath;
begin
  lpath := TListPath.Create;
  try
    lpath.LoadDatas;
//    lpath.LoadSample;
    lpath.ListPaths;
  finally
    lpath.Free;
  end;
end;

constructor TListPath.Create;
begin
  FCaves := TStringList.Create;
  FLinks := TDictionary<string, string>.Create;
  FNumPath := 0;
end;

destructor TListPath.Destroy;
begin
  FLinks.Free;
  FCaves.Free;
  inherited;
end;

procedure TListPath.AddCaves(const s1, s2: string);
begin
  if (FCaves.IndexOf(s1) < 0) then
  begin
    FCaves.Add(s1);
    FLinks.Add(s1, s2);
  end
  else
    FLinks[s1] := FLinks[s1] + '-' + s2;

  if (FCaves.IndexOf(s2) < 0) then
  begin
    FCaves.Add(s2);
    FLinks.Add(s2, s1);
  end
  else
    FLinks[s2] := FLinks[s2] + '-' + s1;
end;

procedure TListPath.LoadDatas(const AFilename: string = '');
const
  FILE_NAME = '..\..\input.txt';
var
  lst: TStringList;
  str, s1, s2: string;
  i, idx: Integer;
begin
  lst := TStringList.Create;
  try
    if (AFilename.IsEmpty) then
      lst.LoadFromFile(FILE_NAME)
    else
      lst.LoadFromFile(AFilename);

    for i:=0 to lst.Count-1 do
    begin
      str := lst[i];
      idx := str.IndexOf('-');

      s1 := Copy(str, 1, idx);
      s2 := Copy(str, idx+2, Length(str));

      AddCaves(s1, s2);
    end;

    Writeln('Caves: ' + FCaves.Count.ToString);
  finally
    lst.Free;
  end;
end;

procedure TListPath.LoadSample;
const
  SAMPLE1 = '..\..\sample1.txt';
  SAMPLE2 = '..\..\sample2.txt';
  SAMPLE3 = '..\..\sample3.txt';
begin
  LoadDatas(SAMPLE3);
end;

procedure TListPath.ListPaths;
var
  caves: TStringList;
begin
  caves := TStringList.Create;
  try
    GetPath(caves, 'start');
  finally
    caves.Free;
  end;
end;

procedure TListPath.GetPath(ACurrent: TStringList; AName: string);
var
  lst: TStringList;
  str, newPath: string;
begin
  lst := TStringList.Create;
  try
    lst.Delimiter := '-';
    lst.DelimitedText := FLinks[AName];

    ACurrent.Add(AName);

    for str in lst do
    begin
      if (str = 'start') then
        Continue
      else if (str = 'end') then
      begin
        ACurrent.Add(str);
        FNumPath := FNumPath + 1;
        Writeln(FNumPath.ToString + ': ' + ACurrent.DelimitedText);
        ACurrent.Delete(ACurrent.Count - 1);
      end
      else
      begin
        if (str.Chars[0] in ['a'..'z'])
          and (ACurrent.IndexOf(str) >= 0) then
          Continue;

        GetPath(ACurrent, str);
      end;
    end;

    ACurrent.Delete(ACurrent.Count - 1);
  finally
    lst.Free;
  end;
end;

end.
