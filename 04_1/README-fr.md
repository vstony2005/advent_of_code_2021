# Day 4 [1]

Traduit avec www.DeepL.com/Translator (version gratuite)

--- Jour 4 : Calmar géant ---

Tu es déjà à presque 1,5 km (presque un mile) sous la surface de l'océan, déjà si profond que tu ne peux pas voir la lumière du soleil. Ce que tu peux voir, cependant, c'est un calmar géant qui s'est attaché à l'extérieur de ton sous-marin.

Peut-être veut-il jouer au bingo ?

Le bingo se joue sur un ensemble de tableaux comprenant chacun une grille de chiffres de 5 x 5. Les numéros sont choisis au hasard, et le numéro choisi est marqué sur tous les tableaux sur lesquels il apparaît. (Si tous les numéros d'une ligne ou d'une colonne d'un tableau sont marqués, ce tableau est gagnant. (Les diagonales ne comptent pas.)

Le sous-marin possède un sous-système de bingo pour aider les passagers (actuellement, vous et le calmar géant) à passer le temps. Il génère automatiquement un ordre aléatoire dans lequel tirer des numéros et un ensemble aléatoire de tableaux (votre entrée de puzzle). Par exemple :

    7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

    22 13 17 11 0
    8 2 23 4 24
    21 9 14 16 7
    6 10 3 18 5
    1 12 20 15 19

    3 15 0 2 22
    9 18 13 17 5
    19 8 7 25 23
    20 11 10 24 4
    14 21 16 12 6

    14 21 17 24 4
    10 16 15 9 19
    18 8 23 26 20
    22 11 13 6 5
    2 0 12 3 7

Après le tirage des cinq premiers numéros (7, 4, 9, 5 et 11), il n'y a pas de gagnant, mais les tableaux sont marqués comme suit (représentés ici les uns à côté des autres pour gagner de la place) :

    22 13 17 11  0    3 15  0  2 22     14 21 17 24  4
     8  2 23  4 24    9 18 13 17  5     10 16 15  9 19
    21  9 14 16  7   19  8  7 25 23     18  8 23 26 20
     6 10  3 18  5   20 11 10 24  4     22 11 13  6  5
     1 12 20 15 19   14 21 16 12  6      2  0 12  3  7

Après le tirage des six numéros suivants (17, 23, 2, 0, 14 et 21), il n'y a toujours pas de gagnant :

    22 13 17 11  0    3 15  0  2 22     14 21 17 24  4
     8  2 23  4 24    9 18 13 17  5     10 16 15  9 19
    21  9 14 16  7   19  8  7 25 23     18  8 23 26 20
     6 10  3 18  5   20 11 10 24  4     22 11 13  6  5
     1 12 20 15 19   14 21 16 12  6      2  0 12  3  7

Enfin, on dessine 24 :

    22 13 17 11  0    3 15  0  2 22     14 21 17 24  4
     8  2 23  4 24    9 18 13 17  5     10 16 15  9 19
    21  9 14 16  7   19  8  7 25 23     18  8 23 26 20
     6 10  3 18  5   20 11 10 24  4     22 11 13  6  5
     1 12 20 15 19   14 21 16 12  6      2  0 12  3  7

    >> 7,4,9,5,11,17,23,2,0,14,21,24
        14 21 17 24  4

        14 21 17 24  4
        10 16 15  9 19
        18  8 23 26 20
        22 11 13  6  5
         2  0 12  3  7

        14 21 17 24  4
         -  -  -  9  -
         -  - 23  -  -
         - 11  -  -  5
         2  0  -  -  7

         -  -  -  -  -
        10 16 15  - 19
        18  8  - 26 20  ->  188
        22  - 13  6  -
         -  - 12  3  -

À ce stade, le troisième tableau est gagnant car il possède au moins une ligne ou une colonne complète de chiffres marqués (dans ce cas, la ligne supérieure entière est marquée : 14 21 17 24 4).

Le score du tableau gagnant peut maintenant être calculé. Commencez par trouver la somme de tous les numéros non marqués sur ce plateau ; dans ce cas, la somme est 188. Ensuite, multipliez cette somme par le nombre qui vient d'être appelé lorsque le tableau a gagné, 24, pour obtenir le score final, 188 * 24 = 4512.

Pour garantir la victoire contre le calmar géant, déterminez quel tableau va gagner en premier. Quel sera votre score final si vous choisissez ce plateau ?

To begin, get your [puzzle input](input.txt).