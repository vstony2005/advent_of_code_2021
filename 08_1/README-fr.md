# Day 8 [1]

--- Jour 8 : Sept segments de recherche ---

Vous atteignez à peine la sécurité de la grotte que la baleine s'écrase contre l'entrée de la grotte, la faisant s'effondrer. Les détecteurs indiquent une autre sortie de la grotte à une profondeur beaucoup plus grande, vous n'avez donc pas d'autre choix que de continuer.

Alors que votre sous-marin se fraye lentement un chemin à travers le système de grottes, vous remarquez que les [affichages à sept segments] (https://en.wikipedia.org/wiki/Seven-segment_display) à quatre chiffres de votre sous-marin fonctionnent mal ; ils ont dû être endommagés pendant l'évasion. Vous aurez de gros problèmes sans eux, alors vous feriez mieux de trouver ce qui ne va pas.

Chaque chiffre d'un affichage à sept segments est rendu en activant ou désactivant l'un des sept segments nommés de a à g :

      0 :       1 :       2 :       3 :       4 :
     aaaa      ....      aaaa      aaaa      ....
    b    c    .    c    .    c    .    c    b    c
    b    c    .    c    .    c    .    c    b    c
     ....      ....      dddd      dddd      dddd
    e    f    .    f    e   .     .    f    .    f
    e    f    .    f    e   .     .    f    .    f
     gggg      ....      gggg      gggg      ....

      5 :       6 :       7 :       8 :       9 :
     aaaa      aaaa      aaaa      aaaa      aaaa
    b    .    b    .    .    c    b    c    b    c
    b    .    b    .    .    c    b    c    b    c
     dddd      dddd      ....      dddd      dddd
    .    f    e    f    .    f    e    f    .    f
    .    f    e    f    .    f    e    f    .    f
     gggg      gggg      ....      gggg      gggg

Ainsi, pour obtenir un 1, seuls les segments c et f sont activés, les autres sont désactivés. Pour obtenir un 7, seuls les segments a, c et f sont activés.

Le problème est que les signaux qui commandent les segments ont été mélangés sur chaque écran. Le sous-marin essaie toujours d'afficher des nombres en produisant une sortie sur les fils de signaux a à g, mais ces fils sont connectés aux segments de manière aléatoire. Pire encore, les connexions fil/segment sont mélangées séparément pour chaque affichage à quatre chiffres ! (Tous les chiffres d'un affichage utilisent cependant les mêmes connexions).

Ainsi, vous pouvez savoir que seuls les fils de signal b et g sont allumés, mais cela ne signifie pas que les segments b et g sont allumés : le seul chiffre qui utilise deux segments est le 1, ce qui signifie que les segments c et f sont censés être allumés. Avec cette seule information, vous ne pouvez toujours pas dire quel fil (b/g) va à quel segment (c/f). Pour cela, vous devez collecter d'autres informations.

Pour chaque écran, observez les signaux changeants pendant un certain temps, notez les dix modèles de signaux uniques que vous voyez, puis notez une seule valeur de sortie à quatre chiffres (l'entrée de votre puzzle). À l'aide des modèles de signaux, vous devriez être en mesure de déterminer quel modèle correspond à quel chiffre.

Par exemple, voici ce que vous pourriez voir dans une seule entrée de vos notes :

    acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
    cdfeb fcadb cdfeb cdbaf

(L'entrée est enveloppée ici sur deux lignes pour qu'elle tienne ; dans vos notes, elle sera sur une seule ligne).

Chaque entrée se compose de dix modèles de signaux uniques, d'un délimiteur | et enfin de la valeur de sortie à quatre chiffres. Au sein d'une entrée, les mêmes connexions de fils/segments sont utilisées (mais vous ne savez pas quelles sont ces connexions). Les modèles de signaux uniques correspondent aux dix façons différentes dont le sous-marin essaie de restituer un chiffre en utilisant les connexions filaires/segmentaires actuelles. Parce que 7 est le seul chiffre qui utilise trois segments, dab dans l'exemple ci-dessus signifie que pour rendre un 7, les lignes de signal d, a et b sont activées. Comme 4 est le seul chiffre qui utilise quatre segments, eafb signifie que pour obtenir un 4, les lignes de signal e, a, f et b sont activées.

Grâce à ces informations, vous devriez être en mesure de déterminer quelle combinaison de fils de signaux correspond à chacun des dix chiffres. Ensuite, vous pouvez décoder la valeur de sortie à quatre chiffres. Malheureusement, dans l'exemple ci-dessus, tous les chiffres de la valeur de sortie (cdfeb fcadb cdfeb cdbaf) utilisent cinq segments et sont plus difficiles à déduire.

Pour l'instant, concentrez-vous sur les chiffres faciles à déduire. Considérez cet exemple plus large :

    be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb |
    fdgacbe cefdb cefbgd gcbe
    edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec |
    fcgedb cgb dgebacf gc
    fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef |
    cg cg fdcagb cbg
    fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega |
    efabcd cedba gadfec cb
    aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga |
    gecf egdcabf bgf bfgea
    fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf |
    gebdcfa ecba ca fadegcb
    dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf |
    cefg dcbef fcge gbcadfe
    bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd |
    ed bcgafe cdgba cbgef
    egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg |
    gbdfcae bgc cg cgb
    gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc |
    fgae cfgab fg bagce

Comme les chiffres 1, 4, 7 et 8 utilisent chacun un nombre unique de segments, vous devriez être en mesure de dire quelles combinaisons de signaux correspondent à ces chiffres. En comptant uniquement les chiffres dans les valeurs de sortie (la partie après | sur chaque ligne), dans l'exemple ci-dessus, il y a 26 occurrences de chiffres qui utilisent un nombre unique de segments (mis en évidence ci-dessus).

Dans les valeurs de sortie, combien de fois les chiffres 1, 4, 7 ou 8 apparaissent-ils ?

[Datas](input.txt)
